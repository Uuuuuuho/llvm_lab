
#include "all_short.h"

//=============
//  COMMIT
//=============


void floyds(int p[][N], int n, int via[][N]) {
  int i, j, k;
#if FACTOR == INNER_MOST
  int *t_1, t_2;
#endif

  for (k = 1; k < n; k++)
#if UNROLL_FACTOR == 2
    for (i = 1; i < n; i += 2)
      for (j = 1; j < n; j += 2) {
        // #if FACTOR == INNER_MOST
        //       t_1 = &p[i][j];
        // #endif
#elif UNROLL_FACTOR == 4
    for (i = 1; i < n; i += 4)
      for (j = 1; j < n; j += 4) {
#elif UNROLL_FACTOR == 1
    for (i = 1; i < n; i++)
      for (j = 1; j < n; j++) {

#endif
#if UNROLL_FACTOR == 2
        if (i == j) {
          p[i][j] = INF;
          p[i + 1][j + 1] = INF;

          // for correctness
          if (p[i][j + 1] > p[i][k] + p[k][j + 1])
            via[i][j + 1] = k;
          if (p[i + 1][j] > p[i + 1][k] + p[k][j])
            via[i + 1][j] = k;
          p[i][j + 1] = min(p[i][j + 1], p[i][k] + p[k][j + 1]);
          p[i + 1][j] = min(p[i + 1][j], p[i + 1][k] + p[k][j]);
        } else {

          if (p[i][j] > p[i][k] + p[k][j])
            via[i][j] = k;
          if (p[i][j + 1] > p[i][k] + p[k][j + 1])
            via[i][j + 1] = k;
          if (p[i + 1][j] > p[i + 1][k] + p[k][j])
            via[i + 1][j] = k;
          if (p[i + 1][j + 1] > p[i + 1][k] + p[k][j + 1])
            via[i + 1][j + 1] = k;

          p[i][j] = min(p[i][j], p[i][k] + p[k][j]);
          p[i][j + 1] = min(p[i][j + 1], p[i][k] + p[k][j + 1]);
          p[i + 1][j] = min(p[i + 1][j], p[i + 1][k] + p[k][j]);
          p[i + 1][j + 1] = min(p[i + 1][j + 1], p[i + 1][k] + p[k][j + 1]);
        }

#elif UNROLL_FACTOR == 4
        if (i == j) {
          p[i][j] = INF;
          p[i + 1][j + 1] = INF;
          p[i + 2][j + 2] = INF;
          p[i + 3][j + 3] = INF;

          // for unroll j+=2
          // if (p[i+1][j]   > p[i+1][k]   + p[k][j])     via[i+1][j] = k;
          // p[i+1][j]   = min(p[i+1][j]  , p[i+1][k]   + p[k][j]);

          // for correctness
          if (p[i][j + 1] > p[i][k] + p[k][j + 1])
            via[i][j + 1] = k;
          if (p[i][j + 2] > p[i][k] + p[k][j + 2])
            via[i][j + 2] = k;
          if (p[i][j + 3] > p[i][k] + p[k][j + 3])
            via[i][j + 3] = k;
          if (p[i + 1][j] > p[i + 1][k] + p[k][j])
            via[i + 1][j] = k;
          if (p[i + 1][j + 2] > p[i + 1][k] + p[k][j + 2])
            via[i + 1][j + 2] = k;
          if (p[i + 1][j + 3] > p[i + 1][k] + p[k][j + 3])
            via[i + 1][j + 3] = k;
          if (p[i + 2][j] > p[i + 2][k] + p[k][j])
            via[i + 2][j] = k;
          if (p[i + 2][j + 1] > p[i + 2][k] + p[k][j + 1])
            via[i + 2][j + 1] = k;
          if (p[i + 2][j + 3] > p[i + 2][k] + p[k][j + 3])
            via[i + 2][j + 3] = k;
          if (p[i + 3][j] > p[i + 3][k] + p[k][j])
            via[i + 3][j] = k;
          if (p[i + 3][j + 1] > p[i + 3][k] + p[k][j + 1])
            via[i + 3][j + 1] = k;
          if (p[i + 3][j + 2] > p[i + 3][k] + p[k][j + 2])
            via[i + 3][j + 2] = k;
          p[i][j + 1] = min(p[i][j + 1], p[i][k] + p[k][j + 1]);
          p[i][j + 2] = min(p[i][j + 2], p[i][k] + p[k][j + 2]);
          p[i][j + 3] = min(p[i][j + 3], p[i][k] + p[k][j + 3]);
          p[i + 1][j] = min(p[i + 1][j], p[i + 1][k] + p[k][j]);
          p[i + 1][j + 2] = min(p[i + 1][j + 2], p[i + 1][k] + p[k][j + 2]);
          p[i + 1][j + 3] = min(p[i + 1][j + 3], p[i + 1][k] + p[k][j + 3]);
          p[i + 2][j] = min(p[i + 2][j], p[i + 2][k] + p[k][j]);
          p[i + 2][j + 1] = min(p[i + 2][j + 1], p[i + 2][k] + p[k][j + 1]);
          p[i + 2][j + 3] = min(p[i + 2][j + 3], p[i + 2][k] + p[k][j + 3]);
          p[i + 3][j] = min(p[i + 3][j], p[i + 3][k] + p[k][j]);
          p[i + 3][j + 1] = min(p[i + 3][j + 1], p[i + 3][k] + p[k][j + 1]);
          p[i + 3][j + 2] = min(p[i + 3][j + 2], p[i + 3][k] + p[k][j + 2]);
          // if (p[i+1][j]   > p[i+1][k+1] + p[k+1][j])     via[i+1][j] = k+1;
          // p[i+1][j]   = min(p[i+1][j]  , p[i+1][k+1]   + p[k+1][j]);

          // if ((i==1) ){
          //   printf("k: %d\t", k);
          //   printf("p[2][7]: %d\t", p[2][7]);
          //   printf("p[2][8]: %d\t", p[2][8]);
          //   printf("p[7][1]: %d\t", p[7][1]);
          //   printf("p[8][1]: %d\n", p[8][1]);
          // }
        }
        // else if ((i - 1) == j) {
        //   if (p[i][j] > p[i][k] + p[k][j]) via[i][j] = k;
        //   p[i][j] = min(p[i][j], p[i][k] + p[k][j]);
        // }
        else {

          if (p[i][j] > p[i][k] + p[k][j])
            via[i][j] = k;
          if (p[i][j + 1] > p[i][k] + p[k][j + 1])
            via[i][j + 1] = k;
          if (p[i][j + 2] > p[i][k] + p[k][j + 2])
            via[i][j + 2] = k;
          if (p[i][j + 3] > p[i][k] + p[k][j + 3])
            via[i][j + 3] = k;
          if (p[i + 1][j] > p[i + 1][k] + p[k][j])
            via[i + 1][j] = k;
          if (p[i + 1][j + 1] > p[i + 1][k] + p[k][j + 1])
            via[i + 1][j + 1] = k;
          if (p[i + 1][j + 2] > p[i + 1][k] + p[k][j + 2])
            via[i + 1][j + 2] = k;
          if (p[i + 1][j + 3] > p[i + 1][k] + p[k][j + 3])
            via[i + 1][j + 3] = k;
          if (p[i + 2][j] > p[i + 2][k] + p[k][j])
            via[i + 2][j] = k;
          if (p[i + 2][j + 1] > p[i + 2][k] + p[k][j + 1])
            via[i + 2][j + 1] = k;
          if (p[i + 2][j + 2] > p[i + 2][k] + p[k][j + 2])
            via[i + 2][j + 2] = k;
          if (p[i + 2][j + 3] > p[i + 2][k] + p[k][j + 3])
            via[i + 2][j + 3] = k;
          if (p[i + 3][j] > p[i + 3][k] + p[k][j])
            via[i + 3][j] = k;
          if (p[i + 3][j + 1] > p[i + 3][k] + p[k][j + 1])
            via[i + 3][j + 1] = k;
          if (p[i + 3][j + 2] > p[i + 3][k] + p[k][j + 2])
            via[i + 3][j + 2] = k;
          if (p[i + 3][j + 3] > p[i + 3][k] + p[k][j + 3])
            via[i + 3][j + 3] = k;
          // if (p[i][j]     > p[i][k+1]   + p[k+1][j])   via[i][j] = k+1;
          // if (p[i][j+1]   > p[i][k+1]   + p[k+1][j+1]) via[i][j+1] = k+1;
          // if (p[i+1][j]   > p[i+1][k+1] + p[k+1][j])   via[i+1][j] = k+1;
          // if (p[i+1][j+1] > p[i+1][k+1] + p[k+1][j+1]) via[i+1][j+1] = k+1;

          // if ((j==7) ){
          //   printf("k: %d, i: %d, j: %d\t", k,i,j);
          //   printf("p[2][7]: %d\t", p[2][7]);
          //   printf("p[2][8]: %d\t", p[2][8]);
          //   printf("p[7][1]: %d\t", p[7][1]);
          //   printf("p[8][1]: %d\n", p[8][1]);
          // }

          p[i][j] = min(p[i][j], p[i][k] + p[k][j]);
          p[i][j + 1] = min(p[i][j + 1], p[i][k] + p[k][j + 1]);
          p[i][j + 2] = min(p[i][j + 2], p[i][k] + p[k][j + 2]);
          p[i][j + 3] = min(p[i][j + 3], p[i][k] + p[k][j + 3]);
          p[i + 1][j] = min(p[i + 1][j], p[i + 1][k] + p[k][j]);
          p[i + 1][j + 1] = min(p[i + 1][j + 1], p[i + 1][k] + p[k][j + 1]);
          p[i + 1][j + 2] = min(p[i + 1][j + 2], p[i + 1][k] + p[k][j + 2]);
          p[i + 1][j + 3] = min(p[i + 1][j + 3], p[i + 1][k] + p[k][j + 3]);
          p[i + 2][j] = min(p[i + 2][j], p[i + 2][k] + p[k][j]);
          p[i + 2][j + 1] = min(p[i + 2][j + 1], p[i + 2][k] + p[k][j + 1]);
          p[i + 2][j + 2] = min(p[i + 2][j + 2], p[i + 2][k] + p[k][j + 2]);
          p[i + 2][j + 3] = min(p[i + 2][j + 3], p[i + 2][k] + p[k][j + 3]);
          p[i + 3][j] = min(p[i + 3][j], p[i + 3][k] + p[k][j]);
          p[i + 3][j + 1] = min(p[i + 3][j + 1], p[i + 3][k] + p[k][j + 1]);
          p[i + 3][j + 2] = min(p[i + 3][j + 2], p[i + 3][k] + p[k][j + 2]);
          p[i + 3][j + 3] = min(p[i + 3][j + 3], p[i + 3][k] + p[k][j + 3]);
          // p[i][j]     = min(p[i][j]    , p[i][k+1]   + p[k+1][j]);
          // p[i][j+1]   = min(p[i][j+1]  , p[i][k+1]   + p[k+1][j+1]);
          // p[i+1][j]   = min(p[i+1][j]  , p[i+1][k+1] + p[k+1][j]);
          // p[i+1][j+1] = min(p[i+1][j+1], p[i+1][k+1] + p[k+1][j+1]);
        }

#elif UNROLL_FACTOR == 1
        if (i == j)
          p[i][j] = INF;
        else {

          if (p[i][j] > p[i][k] + p[k][j])
            via[i][j] = k;

          p[i][j] = min(p[i][j], p[i][k] + p[k][j]);
        }
#endif
      }
}

int min(int a, int b) {
  if (a < b)
    return (a);
  else
    return (b);
}

void find_path(int i, int j, Queue *sol, int via[][N]) {
  if ((via[i][j]) && (i != via[i][j])) {
    find_path(i, via[i][j], sol, via);
    delete (sol);
    find_path(via[i][j], j, sol, via);
  }

  else {
    insert(i, sol);
    insert(j, sol);
    return;
  }
}

void init_queue(Queue *queue) {
  int i, j, k;
  for (i = 1; i < N; i++) {
    queue->q[i] = -1;
    queue->front = -1;
    queue->rear = -1;
    queue->cost = INF;
  }
}

void insert(int input, Queue *queue) {
  int element;
  if (queue->rear == (N - 1))
    return;
  else {
    if (queue->front == -1) {
      queue->front = 0;
      element = input;
      queue->rear++;
      queue->q[queue->rear] = element;
    } else {
      element = input;
      queue->rear++;
      queue->q[queue->rear] = element;
    }
  }
}

void delete (Queue *queue) {
  if (queue->front == -1 || queue->front > queue->rear)
    return;
  else {
    queue->rear--;
  }
}

#if MODE == X86
void print_queue(int *queue) {
  int i;
  for (i = 1; i < N - 1; i++)
    printf("%d =>", queue[i]);
  printf("%d", queue[i]);
}
#endif

#if MODE == RISCV

int main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  Queue solution[N][N];
  INF = 999;

  /* matrix initialization */

#if UNROLL_FACTOR == 1
  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        graph[i][j] = INF;

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;

  /* distance graph initialization */

  for (i = 1; i < N; i++) {
    graph[i][i + 1] = 1;
  }
  for (i = 1; i < N; i++) {
    via[i][i + 1] = i;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  /* solution initialization */

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {

      init_queue(&solution[i][j]);
      insert(i, &solution[i][j]);
    }
  }

  floyds(graph, N, via);

  /* find path for each node */
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      find_path(i, j, &solution[i][j], via);
    }
  }

  /* cost for each path */

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      solution[i][j].cost = graph[i][j];
    }
  }
#elif UNROLL_FACTOR == 2
  for (i = 1; i < N; i+=2)
    for (j = 1; j < N; j+=2)
      for (k = 1; k < N; k+=2){
        graph[i][j] = INF;
        graph[i][j+1] = INF;
        graph[i+1][j] = INF;
        graph[i+1][j+1] = INF;

      } // useless loop

  for (i = 1; i < N; i+=2)
    for (j = 1; j < N; j+=2)
      for (k = 1; k < N; k+=2){
        via[i][j] = FALSE;
        via[i][j+1] = FALSE;
        via[i+1][j] = FALSE;
        via[i+1][j+1] = FALSE;

      } // useless loop

  /* distance graph initialization */

  for (i = 1; i < N; i+=2) {
    graph[i][i + 1] = 1;
    graph[i+1][i + 2] = 1;
  }
  for (i = 1; i < N; i+=2) {
    via[i][i + 1] = i;
    via[i+1][i + 2] = i+1;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  /* solution initialization */

  for (i = 1; i < N; i += 2) {
    for (j = 1; j < N; j += 2) {

      init_queue(&solution[i][j]);
      init_queue(&solution[i][j + 1]);
      init_queue(&solution[i + 1][j]);
      init_queue(&solution[i][j + 1]);
    }
  }
  for (i = 1; i < N; i += 2) {
    for (j = 1; j < N; j += 2) {
      insert(i, &solution[i][j]);
      insert(i, &solution[i][j + 1]);
      insert(i + 1, &solution[i + 1][j]);
      insert(i + 1, &solution[i][j + 1]);
    }
  }

  floyds(graph, N, via);

  /* find path for each node */
  for (i = 1; i < N; i+=2) {
    for (j = 1; j < N; j+=2) {
      find_path(i, j, &solution[i][j], via);
      find_path(i, j+1, &solution[i][j+1], via);
      find_path(i+1, j, &solution[i+1][j], via);
      find_path(i+1, j+1, &solution[i+1][j+1], via);
    }
  }

  /* cost for each path */

  for (i = 1; i < N; i+=2) {
    for (j = 1; j < N; j+=2) {
      solution[i][j].cost = graph[i][j];
      solution[i][j+1].cost = graph[i][j+1];
      solution[i+1][j].cost = graph[i+1][j];
      solution[i+1][j+1].cost = graph[i+1][j+1];
    }
  }

#elif UNROLL_FACTOR == 4
  for (i = 1; i < N; i+=4)
    for (j = 1; j < N; j+=4)
      for (k = 1; k < N; k+=4) {
        graph[i][j] = INF;
        graph[i][j+1] = INF;
        graph[i][j+2] = INF;
        graph[i][j+3] = INF;
        graph[i+1][j] = INF;
        graph[i+1][j+1] = INF;
        graph[i+1][j+2] = INF;
        graph[i+1][j+3] = INF;
        graph[i+2][j] = INF;
        graph[i+2][j+1] = INF;
        graph[i+2][j+2] = INF;
        graph[i+2][j+3] = INF;
        graph[i+3][j] = INF;
        graph[i+3][j+1] = INF;
        graph[i+3][j+2] = INF;
        graph[i+3][j+3] = INF;

      }// useless loop

  for (i = 1; i < N; i+=4)
    for (j = 1; j < N; j+=4)
      for (k = 1; k < N; k+=4){
        via[i][j] = FALSE;
        via[i][j+1] = FALSE;
        via[i][j+2] = FALSE;
        via[i][j+3] = FALSE;
        via[i+1][j] = FALSE;
        via[i+1][j+1] = FALSE;
        via[i+1][j+2] = FALSE;
        via[i+1][j+3] = FALSE;
        via[i+2][j] = FALSE;
        via[i+2][j+1] = FALSE;
        via[i+2][j+2] = FALSE;
        via[i+2][j+3] = FALSE;
        via[i+3][j] = FALSE;
        via[i+3][j+1] = FALSE;
        via[i+3][j+2] = FALSE;
        via[i+3][j+3] = FALSE;

      } // useless loop

  /* distance graph initialization */

  for (i = 1; i < N; i+=4) {
    graph[i][i + 1] = 1;
    graph[i+1][i + 2] = 1;
    graph[i+2][i + 3] = 1;
    graph[i+3][i + 4] = 1;
  }
  for (i = 1; i < N; i+=4) {
    via[i][i + 1] = i;
    via[i+1][i + 2] = i+1;
    via[i+2][i + 3] = i+2;
    via[i+3][i + 4] = i+3;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  /* solution initialization */

  for (i = 1; i < N; i += 4) {
    for (j = 1; j < N; j += 4) {

      init_queue(&solution[i][j]);
      init_queue(&solution[i][j + 1]);
      init_queue(&solution[i][j + 2]);
      init_queue(&solution[i][j + 3]);
      init_queue(&solution[i + 1][j]);
      init_queue(&solution[i + 1][j + 1]);
      init_queue(&solution[i + 1][j + 2]);
      init_queue(&solution[i + 1][j + 3]);
      init_queue(&solution[i + 2][j]);
      init_queue(&solution[i + 2][j + 1]);
      init_queue(&solution[i + 2][j + 2]);
      init_queue(&solution[i + 2][j + 3]);
      init_queue(&solution[i + 3][j]);
      init_queue(&solution[i + 3][j + 1]);
      init_queue(&solution[i + 3][j + 2]);
      init_queue(&solution[i + 3][j + 3]);
    }
  }
  for (i = 1; i < N; i += 4) {
    for (j = 1; j < N; j += 4) {
      insert(i, &solution[i][j]);
      insert(i, &solution[i][j + 1]);
      insert(i, &solution[i][j + 2]);
      insert(i, &solution[i][j + 3]);
      insert(i, &solution[i + 1][j]);
      insert(i, &solution[i + 1][j + 1]);
      insert(i, &solution[i + 1][j + 2]);
      insert(i, &solution[i + 1][j + 3]);
      insert(i, &solution[i + 2][j]);
      insert(i, &solution[i + 2][j + 1]);
      insert(i, &solution[i + 2][j + 2]);
      insert(i, &solution[i + 2][j + 3]);
      insert(i, &solution[i + 3][j]);
      insert(i, &solution[i + 3][j + 1]);
      insert(i, &solution[i + 3][j + 2]);
      insert(i, &solution[i + 3][j + 3]);
    }
  }

  floyds(graph, N, via);

  /* find path for each node */
  for (i = 1; i < N; i+=4) {
    for (j = 1; j < N; j+=4) {
      find_path(i, j, &solution[i][j], via);
      find_path(i, j+1, &solution[i][j+1], via);
      find_path(i, j+2, &solution[i][j+2], via);
      find_path(i, j+3, &solution[i][j+3], via);
      find_path(i+1, j, &solution[i][j], via);
      find_path(i+1, j+1, &solution[i][j+1], via);
      find_path(i+1, j+2, &solution[i][j+2], via);
      find_path(i+1, j+3, &solution[i][j+3], via);
      find_path(i+2, j, &solution[i][j], via);
      find_path(i+2, j+1, &solution[i][j+1], via);
      find_path(i+2, j+2, &solution[i][j+2], via);
      find_path(i+2, j+3, &solution[i][j+3], via);
      find_path(i+3, j, &solution[i][j], via);
      find_path(i+3, j+1, &solution[i][j+1], via);
      find_path(i+3, j+2, &solution[i][j+2], via);
      find_path(i+3, j+3, &solution[i][j+3], via);
    }
  }

  /* cost for each path */

  for (i = 1; i < N; i+=4) {
    for (j = 1; j < N; j+=4) {
      solution[i][j].cost = graph[i][j];
      solution[i][j+1].cost = graph[i][j+1];
      solution[i][j+2].cost = graph[i][j+2];
      solution[i][j+3].cost = graph[i][j+3];
      solution[i+1][j].cost = graph[i+1][j];
      solution[i+1][j+1].cost = graph[i+1][j+1];
      solution[i+1][j+2].cost = graph[i+1][j+2];
      solution[i+1][j+3].cost = graph[i+1][j+3];
      solution[i+2][j].cost = graph[i+2][j];
      solution[i+2][j+1].cost = graph[i+2][j+1];
      solution[i+2][j+2].cost = graph[i+2][j+2];
      solution[i+2][j+3].cost = graph[i+2][j+3];
      solution[i+3][j].cost = graph[i+3][j];
      solution[i+3][j+1].cost = graph[i+3][j+1];
      solution[i+3][j+2].cost = graph[i+3][j+2];
      solution[i+3][j+3].cost = graph[i+3][j+3];
    }
  }
#endif

}

#endif

#if MODE == X86
#if PRINT == FLOYD
/* check the floyd warshall algorithm on x86 */
void main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  INF = 999;

  /* matrix initialization */

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      graph[i][j] = INF;
  }

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;

  /* distance graph initialization */

  for (i = 1; i < N; i++) {
    graph[i][i + 1] = 1;
  }

  printf("\n Matrix of input data:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

  floyds(graph, N, via);

  printf("\n distance graph:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }
}

#elif PRINT == FIND_PATH
/* check the algorithm for kavin bacon problem on x86 */
void main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  INF = 999;

#if UNROLL_FACTOR == 1
  /* matrix initialization */

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        graph[i][j] = INF;

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;

  /* distance graph initialization */

  for (i = 1; i < N; i++) {
    graph[i][i + 1] = 1;
  }
  for (i = 1; i < N; i++) {
    via[i][i + 1] = i;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  floyds(graph, N, via);

  Queue solution[N][N];

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {

      init_queue(&solution[i][j]);
      insert(i, &solution[i][j]);
    }
  }

  printf("\n distance graph:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

  printf("\n via path:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", via[i][j]);
    printf("\n");
  }


  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      find_path(i, j, &solution[i][j], via);
    }
  }

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      solution[i][j].cost = graph[i][j];
    }
  }
#elif UNROLL_FACTOR == 2  
  /* matrix initialization */
  for (i = 1; i < N; i += 2)
    for (j = 1; j < N; j+= 2)
      for (k = 1; k < N; k += 2) {
        graph[i][j] = INF;
        graph[i][j + 1] = INF;
        graph[i + 1][j] = INF;
        graph[i + 1][j + 1] = INF;

      } // useless loop

  for (i = 1; i < N; i += 2)
    for (j = 1; j < N; j += 2)
      for (k = 1; k < N; k += 2) {
        via[i][j] = FALSE;
        via[i][j + 1] = FALSE;
        via[i + 1][j] = FALSE;
        via[i + 1][j + 1] = FALSE;

      } // useless loop

  /* distance graph initialization */

  for (i = 1; i < N; i += 2) {
    graph[i][i + 1] = 1;
    graph[i + 1][i + 2] = 1;
  }
  for (i = 1; i < N; i += 2) {
    via[i][i + 1] = i;
    via[i + 1][i + 2] = i + 1;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  floyds(graph, N, via);

  Queue solution[N][N];

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {

      init_queue(&solution[i][j]);
      insert(i, &solution[i][j]);
    }
  }

  printf("\n distance graph:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

  printf("\n via path:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", via[i][j]);
    printf("\n");
  }

  /* find path for each node */
  for (i = 1; i < N; i += 2) {
    for (j = 1; j < N; j += 2) {
      find_path(i, j, &solution[i][j], via);
      find_path(i, j + 1, &solution[i][j + 1], via);
      find_path(i + 1, j, &solution[i + 1][j], via);
      find_path(i + 1, j + 1, &solution[i + 1][j + 1], via);
    }
  }

  /* cost for each path */

  for (i = 1; i < N; i += 2) {
    for (j = 1; j < N; j += 2) {
      solution[i][j].cost = graph[i][j];
      solution[i][j + 1].cost = graph[i][j + 1];
      solution[i + 1][j].cost = graph[i + 1][j];
      solution[i + 1][j + 1].cost = graph[i + 1][j + 1];
    }
  }
#elif UNROLL_FACTOR == 4
  for (i = 1; i < N; i += 4)
    for (j = 1; j < N; j += 4)
      for (k = 1; k < N; k += 4) {
        graph[i][j] = INF;
        graph[i][j + 1] = INF;
        graph[i][j + 2] = INF;
        graph[i][j + 3] = INF;
        graph[i + 1][j] = INF;
        graph[i + 1][j + 1] = INF;
        graph[i + 1][j + 2] = INF;
        graph[i + 1][j + 3] = INF;
        graph[i + 2][j] = INF;
        graph[i + 2][j + 1] = INF;
        graph[i + 2][j + 2] = INF;
        graph[i + 2][j + 3] = INF;
        graph[i + 3][j] = INF;
        graph[i + 3][j + 1] = INF;
        graph[i + 3][j + 2] = INF;
        graph[i + 3][j + 3] = INF;

      } // useless loop

  for (i = 1; i < N; i += 4)
    for (j = 1; j < N; j += 4)
      for (k = 1; k < N; k += 4) {
        via[i][j] = FALSE;
        via[i][j + 1] = FALSE;
        via[i][j + 2] = FALSE;
        via[i][j + 3] = FALSE;
        via[i + 1][j] = FALSE;
        via[i + 1][j + 1] = FALSE;
        via[i + 1][j + 2] = FALSE;
        via[i + 1][j + 3] = FALSE;
        via[i + 2][j] = FALSE;
        via[i + 2][j + 1] = FALSE;
        via[i + 2][j + 2] = FALSE;
        via[i + 2][j + 3] = FALSE;
        via[i + 3][j] = FALSE;
        via[i + 3][j + 1] = FALSE;
        via[i + 3][j + 2] = FALSE;
        via[i + 3][j + 3] = FALSE;

      } // useless loop

  /* distance graph initialization */

  for (i = 1; i < N; i += 4) {
    graph[i][i + 1] = 1;
    graph[i + 1][i + 2] = 1;
    graph[i + 2][i + 3] = 1;
    graph[i + 3][i + 4] = 1;
  }
  for (i = 1; i < N; i += 4) {
    via[i][i + 1] = i;
    via[i + 1][i + 2] = i + 1;
    via[i + 2][i + 3] = i + 2;
    via[i + 3][i + 4] = i + 3;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  floyds(graph, N, via);

  Queue solution[N][N];

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {

      init_queue(&solution[i][j]);
      insert(i, &solution[i][j]);
    }
  }

  printf("\n distance graph:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

  printf("\n via path:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", via[i][j]);
    printf("\n");
  }

  /* find path for each node */
  for (i = 1; i < N; i += 4) {
    for (j = 1; j < N; j += 4) {
      find_path(i, j, &solution[i][j], via);
      find_path(i, j + 1, &solution[i][j + 1], via);
      find_path(i, j + 2, &solution[i][j + 2], via);
      find_path(i, j + 3, &solution[i][j + 3], via);
      find_path(i + 1, j, &solution[i][j], via);
      find_path(i + 1, j + 1, &solution[i][j + 1], via);
      find_path(i + 1, j + 2, &solution[i][j + 2], via);
      find_path(i + 1, j + 3, &solution[i][j + 3], via);
      find_path(i + 2, j, &solution[i][j], via);
      find_path(i + 2, j + 1, &solution[i][j + 1], via);
      find_path(i + 2, j + 2, &solution[i][j + 2], via);
      find_path(i + 2, j + 3, &solution[i][j + 3], via);
      find_path(i + 3, j, &solution[i][j], via);
      find_path(i + 3, j + 1, &solution[i][j + 1], via);
      find_path(i + 3, j + 2, &solution[i][j + 2], via);
      find_path(i + 3, j + 3, &solution[i][j + 3], via);
    }
  }

  /* cost for each path */

  for (i = 1; i < N; i += 4) {
    for (j = 1; j < N; j += 4) {
      solution[i][j].cost = graph[i][j];
      solution[i][j + 1].cost = graph[i][j + 1];
      solution[i][j + 2].cost = graph[i][j + 2];
      solution[i][j + 3].cost = graph[i][j + 3];
      solution[i + 1][j].cost = graph[i + 1][j];
      solution[i + 1][j + 1].cost = graph[i + 1][j + 1];
      solution[i + 1][j + 2].cost = graph[i + 1][j + 2];
      solution[i + 1][j + 3].cost = graph[i + 1][j + 3];
      solution[i + 2][j].cost = graph[i + 2][j];
      solution[i + 2][j + 1].cost = graph[i + 2][j + 1];
      solution[i + 2][j + 2].cost = graph[i + 2][j + 2];
      solution[i + 2][j + 3].cost = graph[i + 2][j + 3];
      solution[i + 3][j].cost = graph[i + 3][j];
      solution[i + 3][j + 1].cost = graph[i + 3][j + 1];
      solution[i + 3][j + 2].cost = graph[i + 3][j + 2];
      solution[i + 3][j + 3].cost = graph[i + 3][j + 3];
    }
  }
#endif  

  // for (i = 1; i < N; i++) {
  //   for (j = 1; j < N; j++) {
  //     printf("solution[%d][%d]: ", i,j);
  //     print_queue(&solution[i][j]);
  //     printf("\n");

  //   }
  //   printf("\n");
  // }
}
#endif
#endif