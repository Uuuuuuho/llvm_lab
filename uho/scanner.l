%{
#include <stdio.h>
#include "y.tab.h"
%}

%%
#define										return DEFINE;
void										return VOID;
int 										return INT;
if 											return IF;
for 										return FOR;
continue 									return CONTI;

[a-zA-Z_][a-zA-Z_0-9]*						return ID;

"+"|"-"|"*"|"/"|"||"|"=="|"<"|">"|"="|"++"	return OP;
[-+]?[0-9]*\.?[0-9]* 	 					return NUM;
\".*\"										return STR;

"["											return '[';
"]"											return ']';
"{"											return '{';
"}"											return '}';
"("											return '(';
")"											return ')';
";"											return ';';
","											return ',';

\n 		                 	   	 			{yylineno ++; return '\n';}
\/\/.*|\/\*.*\*\/							/* ignore comments 	 */
[ \t]+   		               				/* ignore whitespace */

%%