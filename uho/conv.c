#include "conv.h"

int *conv(int wgt[], int in[], int bias, int len_wgt, int len_in) {
  int nconv = len_wgt + len_in - 1;
  int i, j, wgt_start, in_start, in_end;

  int out[25];

  for (i = 0; i < nconv; i++) {
    in_start = MAX(0, i - len_wgt + 1);
    in_end = MIN(i + 1, len_in);
    wgt_start = MIN(i, len_wgt - 1);
    for (j = in_start; j < in_end; j++) {
      // out[i] = (wgt[wgt_start] * in[j]) + out[i] + bias;
      out[i] = (wgt[wgt_start] * in[j]) + out[i];
      wgt_start -= 1;
    }
  }
  return out;
}

#define IN_SIZE 100
#define WGT_SIZE 10
int main(int argc, char *argv[]) {
  int in[IN_SIZE] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
  int wgt[WGT_SIZE] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
  // 1, 0, 0, 0, 1, 0, 1, 0, 1, 0};
  int bias = 7;
  int *out = conv(wgt, in, bias, 5, 5);
  return 0;
}
