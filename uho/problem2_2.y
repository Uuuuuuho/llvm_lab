%{
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "y.tab.h"
#include <string.h>
extern FILE *yyin;
extern char *yytext;
extern int yylineno;

char * filename;

int yylex(void);
void yyerror(const char *str, ...)
{
	va_list ap;
	va_start(ap, str);

	fprintf(stderr, "%s:%d: error: %s '%s' token \n", filename, yylineno, str, yytext);
	exit(1);
}

%}

%token DEFINE
%token VOID INT
%token IF FOR CONTI
%token MATCH INCR /* avoid being overlapped with = , + */
%token OR
%token '(' ')' '[' ']' '{' '}' ',' ';'
%token '+' '-' '*' '/'
%token '<' '>' '='
%locations

%union {
	int number;
	char *string;
}

%token <number> NUM;
%token <string> ID;
%token <string> STR;

%type<number> value_of_N
%type<string> two_dimension_array
%type<string> constant_N
%type<string> defined_N
%type<string> bool_statement for_loop_parameter

%%

	/************      TO BE UPDATED    ************/
	/* You need to REWRITE your C Syntax, HERE! */

	c_code
		: tokens
		;

	tokens
		: /* empty */
		| '\n' tokens
		| defined_N tokens
		| function_definition tokens
		;
/* because value_of_N is integer, == is required. */
	defined_N
		: DEFINE constant_N value_of_N {printf("\tDefine %s to %d\n", $2, $3);}
		| DEFINE constant_N {printf("\terror: expected 'number' token\n");}
		| DEFINE value_of_N {printf("\terror: expected 'identifier' token\n");}
		| DEFINE constant_N '=' value_of_N {printf("\tsyntax error: '=' token\n");}
		;
	constant_N
		: ID {$$ =$1;}
		;
	value_of_N
		: NUM {$$ = $1;}
		;
	defined_N_by_N
		: INT ID '[' ']' '[' constant_N ']'
			{printf("\tThis matrix is named %s\n", $2);}
		| ID '[' ']' '[' constant_N ']'
			{printf("\terror: expected 'int' token\n");}
		| INT ID '[' ']' '[' ']'
			{printf("\terror: expected 'N' token\n");}
		| INT ID ']' '[' constant_N ']'
			{printf("\terror: expected '[' token\n");}
		| INT ID '[' '[' constant_N ']'
			{printf("\terror: expected ']' token\n");}
		| INT ID '[' ']' constant_N ']'
				{printf("\terror: expected '[' token\n");}
		| INT ID '[' ']' '[' constant_N
				{printf("\terror: expected ']' token\n");}
		;

	function_definition
		: VOID ID '(' function_parameter ')' '{' exp '}'
			{printf("\tFunction name is %s\n", $2);}
		|  ID '(' function_parameter ')' '{' exp '}'
			{printf("\terror: expected 'void' token\n");}
		| VOID ID function_parameter ')' '{' exp '}'
			{printf("\terror: expected '(' token\n");}
		| VOID ID '(' function_parameter '{' exp '}'
			{printf("\terror: expected ')' token\n");}
		| VOID ID '(' function_parameter ')' exp '}'
			{printf("\terror: expected '{' token\n");}
		| VOID ID '(' function_parameter ')' '{' exp
			{printf("\terror: expected '}' token\n");}
		;

	function_parameter
		: defined_N_by_N ',' defined_N_by_N ',' defined_N_by_N
		| defined_N_by_N ',' defined_N_by_N defined_N_by_N
			{printf("\terror: expected ',' token\n");}
		| defined_N_by_N defined_N_by_N ',' defined_N_by_N
			{printf("\terror: expected ',' token\n");}
		;

	exp: /* empty */
		| '\n' exp
		| exp '\n'
		| type_declaration ';' exp
		| type_declaration exp
			{printf("\terror: expected ';' token\n");}
		| for_loop exp
		| assignment ';' exp
		| assignment exp
			{printf("\terror: expected ';' token\n");}
		| if_statement ';' exp
		| if_statement exp
			{printf("\terror: expected ';' token\n");}
		| plus_assignment ';' exp
		| plus_assignment exp
			{printf("\terror: expected ';' token\n");}
		| print_function ';' exp
		| print_function exp
			{printf("\terror: expected ';' token\n");}
		;

	type_declaration
		: INT ID ',' ID ',' ID
			{printf("\t%s, %s, %s is defined as integer\n", $2, $4, $6);}
		| INT ID ',' ID  ID
			{printf("\terror: expected ',' token\n");}
		| INT ID ID ',' ID
			{printf("\terror: expected ',' token\n");}
		| ID ',' ID ',' ID
			{printf("\terror: expected 'int' token\n");}
		;

	for_loop
		: FOR '(' for_loop_parameter ')' '{' exp '}'
			{printf("\tThis For-Loop has local variable %s\n", $3);}
		| FOR for_loop_parameter ')' '{' exp '}'
			{printf("\terror: expected '(' token\n");}
		| FOR '(' for_loop_parameter '{' exp '}'
			{printf("\terror: expected ')' token\n");}
		| FOR '(' for_loop_parameter ')'  exp '}'
			{printf("\terror: expected '{' token\n");}
		| FOR '(' for_loop_parameter ')' '{' exp
			{printf("\terror: expected '}' token\n");}
		;

	for_loop_parameter
		: ID '=' NUM ';' ID '<' ID ';' ID INCR {$$ = $1;}
		| ID '=' NUM ID '<' ID ';' ID INCR
			{printf("\terror: expected ';' token\n");}
		| ID '=' NUM ';' ID '<' ID ID INCR
			{printf("\terror: expected ';' token\n");}
		| ID '=' NUM ';' ID '<' ID ';' ID '+'
			{printf("\terror: expected '++' token\n");}
		;

	assignment
		: two_dimension_array '=' NUM
			{printf("\tInitialize %s to zero\n", $1);}
		| two_dimension_array NUM
			{printf("\terror: expected '=' token\n");}
		;

	if_statement
		: IF '(' bool_statement OR bool_statement ')' '\n' CONTI
			{printf("\tIf %s is zero or %s is zero then continue the loop\n", $3, $5);}
		| IF  bool_statement OR bool_statement ')' '\n' CONTI
			{printf("\terror: expected '(' token\n");}
		| IF '(' bool_statement OR bool_statement '\n' CONTI
			{printf("\terror: expected ')' token\n");}
		| IF '(' bool_statement bool_statement ')' '\n' CONTI
			{printf("\terror: expected 'comparator' token\n");}
		;

	bool_statement
		: two_dimension_array MATCH NUM {$$ = $1;}
		| two_dimension_array '=' NUM
			{printf("\terror: expected '==' token\n");}
		;

	two_dimension_array
		: ID '[' ID ']' '[' ID ']' {$$ = $1;}
		| ID ID ']' '[' ID ']'
			{printf("\terror: expected '[' token\n");}
		| ID '[' ID '[' ID ']'
			{printf("\terror: expected ']' token\n");}
		| ID '[' ID ']' ID ']'
			{printf("\terror: expected '[' token\n");}
		| ID '[' ID ']' '[' ID
			{printf("\terror: expected ']' token\n");}
		;

	plus_assignment
		: two_dimension_array '+''=' two_dimension_array '*' two_dimension_array
			{printf("\t%s += %s * %s\n", $1, $4, $6);}
		| two_dimension_array '=' two_dimension_array '*' two_dimension_array
			{printf("\terror: expected '+=' token\n");}
		| two_dimension_array '+' two_dimension_array '*' two_dimension_array
			{printf("\terror: expected '+=' token\n");}
		;

	print_function
		: ID '(' STR ')'
			{printf("\t%s is printing %s\n", $1, $3);}
		| ID STR ')'
			{printf("\terror: expected '(' token\n");}
		| ID '(' STR
			{printf("\terror: expected ')' token\n");}
		;
%%

int main( int argc, char **argv )
{
	++argv, --argc;  /* skip over program name */
	if ( argc > 0 )
		yyin = fopen( argv[0],"r");
	else
		yyin = stdin;

	filename = argv[0];

	yyparse();
	return 0;
}
