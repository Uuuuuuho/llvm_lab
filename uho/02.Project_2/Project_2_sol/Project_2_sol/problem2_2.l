%{
#include <stdio.h>
#include "y.tab.h"
%}

%%
#define										return DEFINE;
void										return VOID;
int 										return INT;
if 											return IF;
for 										return FOR;
continue 									return CONTI;

[a-zA-Z_][a-zA-Z_0-9]*						return ID;

"="|"+="									return ASSIGN_OP;
"++"										return INC_OP;
"+"|"-"										return ADD_OP;
"*" | "/"									return MUL_OP;
"&&"|"||"									return LOGIC_OP;
"!="|"=="|"<"|">"|"<="|">="					return REL_OP;

[-+]?[0-9]*\.?[0-9]* 	 					return NUM;
\".*\"										return STR;

"["											return '[';
"]"											return ']';
"{"											return '{';
"}"											return '}';
"("											return '(';
")"											return ')';
";"											return ';';
","											return ',';

\n 		                 	   	 			{yylineno ++;}
\/\/.*|\/\*.*\*\/							/* ignore comments 	 */
[ \t]+   		               				/* ignore whitespace */

%%