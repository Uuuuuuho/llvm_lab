%{
#include <stdio.h>
%}

%%

#define|void|int|if|for|continue|#include			printf("keyword ");
[a-zA-Z_][a-zA-Z_0-9]*						printf("identifier ");
"+"|"-"|"*"|"/"|"||"|"=="|"<"|">"|"="|"++"	printf("operator ");
[-+]?[0-9]*\.?[0-9]* 	 					printf("number ");
\".*\"										printf("string ");

"["|"]"|"{"|"}"|";"|","|"("|")"				printf("delimiter ");

\n 		                 	   	 			printf("\n");
\/\/.*|\/\*.*\*\/							printf("comment ");
[ \t]+   		               				/* ignore whitespace */
%%

int main( argc, argv )
int argc;
char **argv;
	{
	++argv, --argc; /* skip over program name */
	if ( argc > 0 )
		yyin = fopen( argv[0], "r" );
	else
		yyin = stdin;
	
	yylex();
 return 0;
	}

