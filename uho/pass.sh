# clang -S -emit-llvm -Xclang -disable-O0-optnone -fno-builtin -march=rv32im -O0 --target=riscv32 loop_test.c
# opt -debug --loop-unroll -unroll-count=8 -S loop_test.ll -o loop_test_opt.ll
# opt -load ../build/lib/LLVMHello.so --loop-unroll -unroll-count=8 -gvn -licm --debug-pass=Structure -S loop_test.ll -o loop_test_opt.ll
# opt -load ../build/lib/LLVMHello.so -enable-new-pm=0 --hello loop_test.ll -o loop_test_opt.ll
# opt -load ../build/lib/LLVMHello.so -enable-new-pm=0 --legacy-hello-world loop_test.ll -o loop_test_opt.ll
# opt -load ../build/lib/LLVMHello.so -enable-new-pm=0 --legacy-opcode-counter -analyze loop_test.ll
# opt -load ../build/lib/LLVMHello.so -enable-new-pm=0 --legacy-inject-func-call loop_test.ll -analyze
# opt -load ../build/lib/LLVMHello.so -enable-new-pm=0 -legacy-mba-sub -S MBA_sub.ll -o MBA_sub_opt.ll
opt -load ../build/lib/LLVMHello.so -enable-new-pm=0 --slsr -S merge_sort.ll -o merge_sort_opt.ll


