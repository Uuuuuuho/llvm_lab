#include <algorithm>
#include <cstdio>
#include <vector>
#define MAX 10000005
using namespace std;

int via[101][101];
vector<int> path;

void find_path(int i, int j) {
  // i에서 j로 가는 최단경로 찾기

  if (via[i][j]) {
    // i와 j 사이의 경유점이 있음
    find_path(i, via[i][j]);
    path.pop_back();
    find_path(via[i][j], j);
  }

  else {
    // i와 j 사이의 경유점이 없음
    path.push_back(i);
    path.push_back(j);
  }
}

int main() {
  int dist[101][101], V, E, a, b, c;
  scanf("%d%d", &V, &E);
  for (int i = 1; i <= V; i++) {
    for (int j = 1; j <= V; j++) {
      if (i == j)
        dist[i][j] = 0;
      else
        dist[i][j] = MAX;
    }
  }
  for (int i = 0; i < E; i++) {
    scanf("%d%d%d", &a, &b, &c);
    dist[a][b] = min(c, dist[a][b]);
  }
  /* dist배열 초기화, 입력받기
  v : 정점의 수 */

  for (int i = 1; i <= V; i++) {
    for (int j = 1; j <= V; j++) {
      for (int k = 1; k <= V; k++) {
        if (dist[j][k] > dist[j][i] + dist[i][k]) {
          dist[j][k] = dist[j][i] + dist[i][k];
          via[j][k] = i;
        }
      }
    }
  }
  for (int i = 1; i <= V; i++) {
    for (int j = 1; j <= V; j++) {
      if (dist[i][j] == MAX)
        printf("0 ");
      else
        printf("%d ", dist[i][j]);
    }
    printf("\n");
  }
  for (int i = 1; i <= V; i++) {
    for (int j = 1; j <= V; j++) {
      if (i == j || dist[i][j] == MAX)
        printf("0\n");
      else {
        path.clear();
        find_path(i, j);
        printf("%d ", path.size());
        for (auto i : path)
          printf("%d ", i);
        printf("\n");
      }
    }
  }
  return 0;
}
