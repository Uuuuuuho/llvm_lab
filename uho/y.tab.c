/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "problem2_2.y" /* yacc.c:339  */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "y.tab.h"
#include <string.h>
extern FILE *yyin;
extern char *yytext;
extern int yylineno;

char * filename;

int yylex(void);
void yyerror(const char *str, ...)
{
	va_list ap;
	va_start(ap, str);

	fprintf(stderr, "%s:%d: error: %s '%s' token \n", filename, yylineno, str, yytext);
	exit(1);
}


#line 90 "y.tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    DEFINE = 258,
    VOID = 259,
    INT = 260,
    IF = 261,
    FOR = 262,
    CONTI = 263,
    MATCH = 264,
    INCR = 265,
    OR = 266,
    NUM = 267,
    ID = 268,
    STR = 269
  };
#endif
/* Tokens.  */
#define DEFINE 258
#define VOID 259
#define INT 260
#define IF 261
#define FOR 262
#define CONTI 263
#define MATCH 264
#define INCR 265
#define OR 266
#define NUM 267
#define ID 268
#define STR 269

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 35 "problem2_2.y" /* yacc.c:355  */

	int number;
	char *string;

#line 163 "y.tab.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


extern YYSTYPE yylval;
extern YYLTYPE yylloc;
int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 194 "y.tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  16
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   262

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  31
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  19
/* YYNRULES -- Number of rules.  */
#define YYNRULES  74
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  213

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   269

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      30,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      12,    13,    22,    20,    18,    21,     2,    23,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    19,
      24,    26,    25,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    14,     2,    15,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    16,     2,    17,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    27,    28,    29
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    56,    56,    59,    61,    62,    63,    67,    68,    69,
      70,    73,    76,    79,    81,    83,    85,    87,    89,    91,
      96,    98,   100,   102,   104,   106,   111,   112,   114,   118,
     119,   120,   121,   122,   124,   125,   126,   128,   129,   131,
     132,   134,   135,   140,   142,   144,   146,   151,   153,   155,
     157,   159,   164,   165,   167,   169,   174,   176,   181,   183,
     185,   187,   192,   193,   198,   199,   201,   203,   205,   210,
     212,   214,   219,   221,   223
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "DEFINE", "VOID", "INT", "IF", "FOR",
  "CONTI", "MATCH", "INCR", "OR", "'('", "')'", "'['", "']'", "'{'", "'}'",
  "','", "';'", "'+'", "'-'", "'*'", "'/'", "'<'", "'>'", "'='", "NUM",
  "ID", "STR", "'\\n'", "$accept", "c_code", "tokens", "defined_N",
  "constant_N", "value_of_N", "defined_N_by_N", "function_definition",
  "function_parameter", "exp", "type_declaration", "for_loop",
  "for_loop_parameter", "assignment", "if_statement", "bool_statement",
  "two_dimension_array", "plus_assignment", "print_function", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,    40,    41,    91,    93,   123,   125,    44,    59,
      43,    45,    42,    47,    60,    62,    61,   267,   268,   269,
      10
};
# endif

#define YYPACT_NINF -73

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-73)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
       0,   109,    38,    95,     0,   122,   -73,     0,     0,   -73,
     -73,   157,   -73,     5,    36,   -73,   -73,   -73,   -73,   104,
     -73,   132,    36,   155,    22,   169,   176,   -73,   171,    76,
     179,    36,   177,   180,   181,   173,   184,    29,    75,   185,
      24,    36,    75,    75,   172,    33,   172,   174,    78,    86,
      75,   121,    75,   115,    32,    75,    66,    69,   154,    72,
     110,   117,   172,    36,   -73,   -73,   126,   129,   186,   136,
     188,   189,   144,   182,    90,   194,    61,   183,   187,   193,
     135,   190,   192,   195,   197,   196,   191,   -73,   -73,    75,
     191,   191,    75,   191,    75,   191,   151,   163,   -73,    75,
     191,    75,   191,   -73,   199,   -73,   -73,   -73,   -73,   -73,
     200,   -73,   -73,   198,   204,    -2,   182,   201,   202,   162,
     203,   208,   -73,   205,   178,   207,   213,   -73,   191,   191,
     191,   182,   209,   -73,   210,   191,   191,   -73,   -73,   145,
     206,   182,   220,   222,   -73,   -73,   114,    75,    27,    75,
     -73,   211,    98,   212,   214,   215,   182,   182,   216,   -73,
     -73,    71,   217,   218,    75,   137,   138,   221,   219,   140,
     223,   224,   226,   -73,   230,   182,   -73,   -73,   -73,   225,
     228,   238,   242,   141,   -73,   -73,   227,   229,   -73,   -73,
     239,   -73,   -73,   -73,   245,   -73,   -73,   -73,   -73,   231,
     237,   -73,   -73,    67,   232,   233,   248,   252,   156,   -73,
     -73,   -73,   -73
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       3,     0,     0,     0,     3,     0,     2,     3,     3,    12,
      11,     8,     9,     0,     0,     4,     1,     5,     6,     0,
       7,     0,     0,     0,     0,     0,     0,    10,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    29,    29,     0,
       0,     0,    29,    29,     0,     0,     0,     0,     0,     0,
      29,     0,    29,     0,    29,    29,    29,    29,     0,    29,
      29,     0,     0,     0,    27,    28,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      25,     0,     0,     0,     0,     0,    30,    24,    31,    29,
      33,    34,    29,    36,    29,    38,     0,     0,    57,    29,
      40,    29,    42,    23,     0,    26,    22,    21,    17,    15,
      19,    18,    16,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    20,    74,     0,     0,     0,    73,    32,    35,
      37,     0,     0,    56,     0,    39,    41,    14,    13,     0,
       0,     0,     0,     0,    62,    63,    29,    29,     0,    29,
      72,     0,     0,     0,     0,     0,     0,     0,     0,    44,
      45,     0,     0,     0,    29,     0,     0,     0,     0,     0,
       0,     0,     0,    46,     0,     0,    71,    70,    43,     0,
       0,     0,     0,    51,    50,    49,     0,     0,    48,    66,
      68,    67,    65,    69,     0,    60,    61,    59,    47,     0,
       0,    64,    58,     0,     0,     0,     0,     0,     0,    54,
      53,    52,    55
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
     -73,   -73,     4,   -73,    79,    -6,    -9,   -73,   139,   -36,
     -73,   -73,   130,   -73,   -73,   -72,   -48,   -73,   -73
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     5,     6,     7,    11,    12,    24,     8,    25,    53,
      54,    55,    79,    56,    57,    75,    58,    59,    60
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      76,   115,    61,     1,     2,    20,    66,    67,    15,   141,
      21,    17,    18,    27,    80,    32,    86,    22,    90,    91,
      93,    95,    40,   100,   102,    76,    74,    21,     3,    21,
       4,    64,    65,    23,    47,    48,    49,    47,    48,    49,
      31,    21,    63,   142,   143,    50,   167,    69,   132,   134,
      23,    89,    23,   128,   105,   168,   129,    51,   130,    52,
      51,    10,    52,   135,    23,   136,    13,    76,    76,   161,
     117,    47,    48,    49,    47,    48,    49,    47,    48,    49,
      47,    48,    49,   155,   179,    92,   205,   118,    94,    37,
      73,    99,    38,    76,    51,   206,    52,    51,    77,    52,
      51,   180,    52,    51,    82,    52,    74,    14,   176,   177,
     165,   166,   171,   169,    78,    47,    48,    49,    84,    47,
      48,    49,    16,    68,    70,    71,   172,   193,   183,   101,
     164,     9,    87,    81,   103,    82,     9,    10,    51,    83,
      52,   104,    51,   106,    52,    88,   107,    88,   110,    84,
      85,   109,   122,    26,   184,   185,    88,   188,   198,    88,
      28,    29,   113,   158,    10,    88,   211,    88,    88,    30,
      88,    88,   114,   159,    96,   146,   212,   131,   147,    74,
      97,    98,    33,    19,     9,    35,    36,    44,    45,    34,
     133,    74,   151,   152,    39,    41,    42,    43,    46,    62,
      10,   108,    72,   111,   112,   116,   121,   119,     0,   127,
      74,    78,   126,   120,   137,   138,     0,     0,   150,   123,
     124,    88,   140,   125,   149,   153,   139,   154,   144,   145,
     148,   156,   157,   162,   160,   163,   195,   175,   189,   170,
     173,   191,   174,   187,   178,   192,   196,   181,   182,   186,
     197,   199,   190,   202,   201,   194,   204,   200,   209,   203,
     207,   208,   210
};

static const yytype_int16 yycheck[] =
{
      48,    73,    38,     3,     4,    11,    42,    43,     4,    11,
       5,     7,     8,    19,    50,    24,    52,    12,    54,    55,
      56,    57,    31,    59,    60,    73,    28,     5,    28,     5,
      30,    40,    41,    28,     5,     6,     7,     5,     6,     7,
      18,     5,    18,   115,   116,    16,    19,    14,    96,    97,
      28,    19,    28,    89,    63,    28,    92,    28,    94,    30,
      28,    28,    30,    99,    28,   101,    28,   115,   116,   141,
       9,     5,     6,     7,     5,     6,     7,     5,     6,     7,
       5,     6,     7,   131,    13,    19,    19,    26,    19,    13,
      12,    19,    16,   141,    28,    28,    30,    28,    12,    30,
      28,    30,    30,    28,    14,    30,    28,    12,   156,   157,
     146,   147,    14,   149,    28,     5,     6,     7,    28,     5,
       6,     7,     0,    44,    45,    46,    28,   175,   164,    19,
      16,    27,    17,    12,    17,    14,    27,    28,    28,    18,
      30,    62,    28,    17,    30,    30,    17,    30,    69,    28,
      29,    15,    17,    14,    17,    17,    30,    17,    17,    30,
      28,    22,    18,    18,    28,    30,    10,    30,    30,    14,
      30,    30,    28,    28,    20,    13,    20,    26,    16,    28,
      26,    27,    13,    26,    27,    14,    15,    14,    15,    13,
      27,    28,    14,    15,    15,    18,    16,    16,    14,    14,
      28,    15,    28,    15,    15,    11,    13,    77,    -1,    13,
      28,    28,    15,    26,    15,    15,    -1,    -1,    13,    29,
      28,    30,    18,    28,    16,    18,    28,    14,    27,    27,
      27,    22,    22,    13,    28,    13,     8,    22,    15,    28,
      28,    15,    28,    24,    28,    15,     8,    30,    30,    28,
       8,    24,    28,     8,    15,    30,    19,    28,    10,    28,
      28,    28,    10
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,    28,    30,    32,    33,    34,    38,    27,
      28,    35,    36,    28,    12,    33,     0,    33,    33,    26,
      36,     5,    12,    28,    37,    39,    39,    36,    28,    39,
      14,    18,    37,    13,    13,    14,    15,    13,    16,    15,
      37,    18,    16,    16,    14,    15,    14,     5,     6,     7,
      16,    28,    30,    40,    41,    42,    44,    45,    47,    48,
      49,    40,    14,    18,    37,    37,    40,    40,    35,    14,
      35,    35,    28,    12,    28,    46,    47,    12,    28,    43,
      40,    12,    14,    18,    28,    29,    40,    17,    30,    19,
      40,    40,    19,    40,    19,    40,    20,    26,    27,    19,
      40,    19,    40,    17,    35,    37,    17,    17,    15,    15,
      35,    15,    15,    18,    28,    46,    11,     9,    26,    43,
      26,    13,    17,    29,    28,    28,    15,    13,    40,    40,
      40,    26,    47,    27,    47,    40,    40,    15,    15,    28,
      18,    11,    46,    46,    27,    27,    13,    16,    27,    16,
      13,    14,    15,    18,    14,    47,    22,    22,    18,    28,
      28,    46,    13,    13,    16,    40,    40,    19,    28,    40,
      28,    14,    28,    28,    28,    22,    47,    47,    28,    13,
      30,    30,    30,    40,    17,    17,    28,    24,    17,    15,
      28,    15,    15,    47,    30,     8,     8,     8,    17,    24,
      28,    15,     8,    28,    19,    19,    28,    28,    28,    10,
      10,    10,    20
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    31,    32,    33,    33,    33,    33,    34,    34,    34,
      34,    35,    36,    37,    37,    37,    37,    37,    37,    37,
      38,    38,    38,    38,    38,    38,    39,    39,    39,    40,
      40,    40,    40,    40,    40,    40,    40,    40,    40,    40,
      40,    40,    40,    41,    41,    41,    41,    42,    42,    42,
      42,    42,    43,    43,    43,    43,    44,    44,    45,    45,
      45,    45,    46,    46,    47,    47,    47,    47,    47,    48,
      48,    48,    49,    49,    49
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     0,     2,     2,     2,     3,     2,     2,
       4,     1,     1,     7,     6,     6,     6,     6,     6,     6,
       8,     7,     7,     7,     7,     7,     5,     4,     4,     0,
       2,     2,     3,     2,     2,     3,     2,     3,     2,     3,
       2,     3,     2,     6,     5,     5,     5,     7,     6,     6,
       6,     6,    10,     9,     9,    10,     3,     2,     8,     7,
       7,     7,     3,     3,     7,     6,     6,     6,     6,     6,
       5,     5,     4,     3,     3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Location data for the lookahead symbol.  */
YYLTYPE yylloc
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 7:
#line 67 "problem2_2.y" /* yacc.c:1646  */
    {printf("\tDefine %s to %d\n", (yyvsp[-1].string), (yyvsp[0].number));}
#line 1502 "y.tab.c" /* yacc.c:1646  */
    break;

  case 8:
#line 68 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected 'number' token\n");}
#line 1508 "y.tab.c" /* yacc.c:1646  */
    break;

  case 9:
#line 69 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected 'identifier' token\n");}
#line 1514 "y.tab.c" /* yacc.c:1646  */
    break;

  case 10:
#line 70 "problem2_2.y" /* yacc.c:1646  */
    {printf("\tsyntax error: '=' token\n");}
#line 1520 "y.tab.c" /* yacc.c:1646  */
    break;

  case 11:
#line 73 "problem2_2.y" /* yacc.c:1646  */
    {(yyval.string) =(yyvsp[0].string);}
#line 1526 "y.tab.c" /* yacc.c:1646  */
    break;

  case 12:
#line 76 "problem2_2.y" /* yacc.c:1646  */
    {(yyval.number) = (yyvsp[0].number);}
#line 1532 "y.tab.c" /* yacc.c:1646  */
    break;

  case 13:
#line 80 "problem2_2.y" /* yacc.c:1646  */
    {printf("\tThis matrix is named %s\n", (yyvsp[-5].string));}
#line 1538 "y.tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 82 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected 'int' token\n");}
#line 1544 "y.tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 84 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected 'N' token\n");}
#line 1550 "y.tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 86 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '[' token\n");}
#line 1556 "y.tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 88 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ']' token\n");}
#line 1562 "y.tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 90 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '[' token\n");}
#line 1568 "y.tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 92 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ']' token\n");}
#line 1574 "y.tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 97 "problem2_2.y" /* yacc.c:1646  */
    {printf("\tFunction name is %s\n", (yyvsp[-6].string));}
#line 1580 "y.tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 99 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected 'void' token\n");}
#line 1586 "y.tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 101 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '(' token\n");}
#line 1592 "y.tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 103 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ')' token\n");}
#line 1598 "y.tab.c" /* yacc.c:1646  */
    break;

  case 24:
#line 105 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '{' token\n");}
#line 1604 "y.tab.c" /* yacc.c:1646  */
    break;

  case 25:
#line 107 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '}' token\n");}
#line 1610 "y.tab.c" /* yacc.c:1646  */
    break;

  case 27:
#line 113 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ',' token\n");}
#line 1616 "y.tab.c" /* yacc.c:1646  */
    break;

  case 28:
#line 115 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ',' token\n");}
#line 1622 "y.tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 123 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ';' token\n");}
#line 1628 "y.tab.c" /* yacc.c:1646  */
    break;

  case 36:
#line 127 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ';' token\n");}
#line 1634 "y.tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 130 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ';' token\n");}
#line 1640 "y.tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 133 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ';' token\n");}
#line 1646 "y.tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 136 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ';' token\n");}
#line 1652 "y.tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 141 "problem2_2.y" /* yacc.c:1646  */
    {printf("\t%s, %s, %s is defined as integer\n", (yyvsp[-4].string), (yyvsp[-2].string), (yyvsp[0].string));}
#line 1658 "y.tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 143 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ',' token\n");}
#line 1664 "y.tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 145 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ',' token\n");}
#line 1670 "y.tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 147 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected 'int' token\n");}
#line 1676 "y.tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 152 "problem2_2.y" /* yacc.c:1646  */
    {printf("\tThis For-Loop has local variable %s\n", (yyvsp[-4].string));}
#line 1682 "y.tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 154 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '(' token\n");}
#line 1688 "y.tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 156 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ')' token\n");}
#line 1694 "y.tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 158 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '{' token\n");}
#line 1700 "y.tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 160 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '}' token\n");}
#line 1706 "y.tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 164 "problem2_2.y" /* yacc.c:1646  */
    {(yyval.string) = (yyvsp[-9].string);}
#line 1712 "y.tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 166 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ';' token\n");}
#line 1718 "y.tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 168 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ';' token\n");}
#line 1724 "y.tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 170 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '++' token\n");}
#line 1730 "y.tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 175 "problem2_2.y" /* yacc.c:1646  */
    {printf("\tInitialize %s to zero\n", (yyvsp[-2].string));}
#line 1736 "y.tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 177 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '=' token\n");}
#line 1742 "y.tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 182 "problem2_2.y" /* yacc.c:1646  */
    {printf("\tIf %s is zero or %s is zero then continue the loop\n", (yyvsp[-5].string), (yyvsp[-3].string));}
#line 1748 "y.tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 184 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '(' token\n");}
#line 1754 "y.tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 186 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ')' token\n");}
#line 1760 "y.tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 188 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected 'comparator' token\n");}
#line 1766 "y.tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 192 "problem2_2.y" /* yacc.c:1646  */
    {(yyval.string) = (yyvsp[-2].string);}
#line 1772 "y.tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 194 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '==' token\n");}
#line 1778 "y.tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 198 "problem2_2.y" /* yacc.c:1646  */
    {(yyval.string) = (yyvsp[-6].string);}
#line 1784 "y.tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 200 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '[' token\n");}
#line 1790 "y.tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 202 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ']' token\n");}
#line 1796 "y.tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 204 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '[' token\n");}
#line 1802 "y.tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 206 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ']' token\n");}
#line 1808 "y.tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 211 "problem2_2.y" /* yacc.c:1646  */
    {printf("\t%s += %s * %s\n", (yyvsp[-5].string), (yyvsp[-2].string), (yyvsp[0].string));}
#line 1814 "y.tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 213 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '+=' token\n");}
#line 1820 "y.tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 215 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '+=' token\n");}
#line 1826 "y.tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 220 "problem2_2.y" /* yacc.c:1646  */
    {printf("\t%s is printing %s\n", (yyvsp[-3].string), (yyvsp[-1].string));}
#line 1832 "y.tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 222 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected '(' token\n");}
#line 1838 "y.tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 224 "problem2_2.y" /* yacc.c:1646  */
    {printf("\terror: expected ')' token\n");}
#line 1844 "y.tab.c" /* yacc.c:1646  */
    break;


#line 1848 "y.tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[1] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 226 "problem2_2.y" /* yacc.c:1906  */


int main( int argc, char **argv )
{
	++argv, --argc;  /* skip over program name */
	if ( argc > 0 )
		yyin = fopen( argv[0],"r");
	else
		yyin = stdin;

	filename = argv[0];

	yyparse();
	return 0;
}
