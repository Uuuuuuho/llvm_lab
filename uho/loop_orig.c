#define LOOP 64

__attribute__((optnone))int loop_test(int bias) {
  int result = 1;
  int a[LOOP];

  for (int i = 0; i < LOOP; i += 1)
    a[i] = 10;

  for (int i = 0; i < LOOP; i += 1){
    result = ((result * a[i]) + bias);
    // result = result / a[i];
  }
  // result = result / a[0];
  // result += result;
  return result;
}


__attribute__((optnone)) int main() {
  int bias = 2;
  int res = loop_test(bias);  
  return res;
}

