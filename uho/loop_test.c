#define LOOP 64

// __attribute__((optnone)) int loop_test() {
int loop_test() {
  int result = 0;
  int a[LOOP];
  int i = 0, j = 0;

// #pragma clang loop unroll(disable)
// #pragma unroll(8)
  for (i = 0; i < LOOP; i += 8){
    a[i] = i;
    // for (j = 0; j < LOOP; j++){
    //   result = 1;
    //   result = 0;
    // }
  }

// #pragma clang loop unroll(full)
// #pragma clang loop unroll(enable)
// #pragma clang loop unroll_count(4)
#pragma unroll(8)
  for (i = 0; i < LOOP; i += 8)
    result = result + a[i];
  
  return result;
}

int redundant(int red_1) {
  int red_2, red_3;
  red_2 = 2;
  red_3 = red_1 + red_2;
  return red_3;
}

__attribute__((optnone)) int main() {
  int ret = loop_test();  
  ret = redundant(ret);
  return ret;
}

// int main() {
//   int a[LOOP];
//   int i = 0;
//   int result = 0;

// // #pragma unroll(4)
//   for(i = 0; i < LOOP; i++)
//     a[i] = 1;

//   for(i = 0; i < LOOP; i++)
//     result += a[i];

//   return 0;
// }
