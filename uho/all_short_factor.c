
#include "all_short.h"

//=============
//  COMMIT
//=============

// unroll by 4 + factoring loop invarient
void floyds(int p[][N], int n, int via[][N]) {
  int i, j, k;
  for (k = 1; k < n; k++) {   // outer loop : Loopdef(k,i,j)
    for (i = 1; i < n; i++) { // second loop : Lookdef(i,j)
      int *t1 = &p[i];
      int *t2 = &via[i];
      for (j = 1; j < n;
           j += 4) { // inner loop : Lookdef(j) -> p[i], v[i]는 밖으로 빼기
        if (i == j)
          t1[j] = INF;
        else if (i == j + 1)
          t1[j + 1] = INF;
        else if (i == j + 2)
          t1[j + 2] = INF;
        else if (i == j + 3)
          t1[j + 3] = INF;

        else {

          if (t1[j] > t1[k] + t1[j])
            t2[j] = k;
          t1[j] = min(t1[j], t1[k] + t1[j]);

          if (t1[j + 1] > t1[k] + t1[j + 1])
            t2[j + 1] = k;
          t1[j + 1] = min(t1[j + 1], t1[k] + t1[j + 1]);

          if (t1[j + 2] > t1[k] + t1[j + 2])
            t2[j + 2] = k;
          t1[j + 2] = min(t1[j + 2], t1[k] + t1[j + 2]);

          if (t1[j + 3] > t1[k] + t1[j + 3])
            t2[j + 3] = k;
          t1[j + 3] = min(t1[j + 3], t1[k] + t1[j + 3]);
        }
      }
    }
  }
}
int min(int a, int b) {
  if (a < b)
    return (a);
  else
    return (b);
}

void find_path(int i, int j, Queue *sol, int via[][N]) {
  if ((via[i][j]) && (i != via[i][j])) {
    find_path(i, via[i][j], sol, via);
    delete (sol);
    find_path(via[i][j], j, sol, via);
  }

  else {
    insert(i, sol);
    insert(j, sol);
    return;
  }
}

// unroll by 4
void init_queue(Queue *queue) {
  int i, j, k;
  for (i = 1; i < N; i += 4) {
    queue->q[i] = -1;
    queue->q[i + 1] = -1;
    queue->q[i + 2] = -1;
    queue->q[i + 3] = -1;
    queue->front = -1;
    queue->rear = -1;
    queue->cost = INF;
  }
}

void insert(int input, Queue *queue) {
  int element;
  if (queue->rear == (N - 1))
    return;
  else {
    if (queue->front == -1) {
      queue->front = 0;
      element = input;
      queue->rear++;
      queue->q[queue->rear] = element;
    } else {
      element = input;
      queue->rear++;
      queue->q[queue->rear] = element;
    }
  }
}

void delete (Queue *queue) {
  if (queue->front == -1 || queue->front > queue->rear)
    return;
  else {
    queue->rear--;
  }
}

#if MODE == X86
void print_queue(int *queue) {
  int i;
  for (i = 1; i < N - 1; i++)
    printf("%d =>", queue[i]);
  printf("%d", queue[i]);
}
#endif

#if MODE == RISCV

int main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  Queue solution[N][N];
  INF = 999;

  /* matrix initialization */

  // unroll by 4
  for (i = 1; i < N; i++)
    for (j = 1; j < N; j += 4)
      for (k = 1; k < N; k++) // useless loop
        graph[i][j] = INF;
  graph[i][j + 1] = INF;
  graph[i][j + 2] = INF;
  graph[i][j + 3] = INF;

  // unroll by 4
  for (i = 1; i < N; i++)
    for (j = 1; j < N; j += 4)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;
  via[i][j + 1] = FALSE;
  via[i][j + 2] = FALSE;
  via[i][j + 3] = FALSE;

  /* distance graph initialization */
  for (i = 1; i < N; i++) {
    graph[i][i + 1] = 1;
  }
  for (i = 1; i < N; i++) {
    via[i][i + 1] = i;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  /* solution initialization */

  // unroll by 4
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j += 4) {

      init_queue(&solution[i][j]);
      init_queue(&solution[i][j + 1]);
      init_queue(&solution[i][j + 2]);
      init_queue(&solution[i][j + 3]);
      insert(i, &solution[i][j]);
      insert(i, &solution[i][j + 1]);
      insert(i, &solution[i][j + 2]);
      insert(i, &solution[i][j + 3]);
    }
  }

  floyds(graph, N, via);

  /* find path for each node */
  // unroll by 4
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j += 4) {
      find_path(i, j, &solution[i][j], via);
      find_path(i, j + 1, &solution[i][j + 1], via);
      find_path(i, j + 2, &solution[i][j + 2], via);
      find_path(i, j + 3, &solution[i][j + 3], via);
    }
  }

  /* cost for each path */
  // unroll by 4
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j += 4) {
      solution[i][j].cost = graph[i][j];
      solution[i][j + 1].cost = graph[i][j + 1];
      solution[i][j + 2].cost = graph[i][j + 2];
      solution[i][j + 3].cost = graph[i][j + 3];
    }
  }
}

#endif

#if MODE == X86
#if PRINT == FLOYD
/* check the floyd warshall algorithm on x86 */
void main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  INF = 999;

  /* matrix initialization */

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        graph[i][j] = INF;

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;

  /* distance graph initialization */

  for (i = 1; i < N; i++) {
    graph[i][i + 1] = 1;
  }

  printf("\n Matrix of input data:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

  floyds(graph, N);

  printf("\n distance graph:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }
}

#elif PRINT == FIND_PATH
/* check the algorithm for kavin bacon problem on x86 */
void main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  INF = 999;

  /* matrix initialization */

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        graph[i][j] = INF;

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;

  /* distance graph initialization */

  for (i = 1; i < N; i++) {
    graph[i][i + 1] = 1;
  }
  for (i = 1; i < N; i++) {
    via[i][i + 1] = i;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  floyds(graph, N, via);

  Queue solution[N][N];

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {

      init_queue(&solution[i][j]);
      insert(i, &solution[i][j]);
    }
  }

  printf("\n distance graph:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

  printf("\n via path:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", via[i][j]);
    printf("\n");
  }

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      find_path(i, j, &solution[i][j], via);
    }
  }

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      solution[i][j].cost = graph[i][j];
    }
  }

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      printf("solution[%d][%d]: ", i, j);
      print_queue(&solution[i][j]);
      printf("\n");
    }
    printf("\n");
  }
}
#endif
#endif