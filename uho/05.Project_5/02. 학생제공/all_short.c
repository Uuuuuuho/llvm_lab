
#include "all_short.h"

//=============
//  COMMIT
//=============

void floyds(int p[][N], int n, int via[][N]) {
  int i, j, k;
  for (k = 1; k < n; k++)
    for (i = 1; i < n; i++)
      for (j = 1; j < n; j++){
        if (i == j)
          p[i][j] = INF;
        else{

          if (p[i][j] >  p[i][k] + p[k][j]) via[i][j] = k;

          p[i][j] = min(p[i][j], p[i][k] + p[k][j]);

        }
      }
}

int min(int a, int b) {
  if (a < b)
    return (a);
  else
    return (b);
}


void find_path(int i, int j, Queue* sol, int via[][N]) {
  if ((via[i][j]) && (i != via[i][j])) {
    find_path(i, via[i][j], sol, via);
    delete (sol);
    find_path(via[i][j], j, sol, via);
  }

  else {
    insert(i, sol);
    insert(j, sol);
    return;
  }
}

void init_queue(Queue *queue){
  int i, j, k;
  for(i = 1; i < N; i++){
    queue->q[i] = -1;
    queue->front = -1;
    queue->rear = -1;
    queue->cost = INF;
  }
}

void insert(int input, Queue *queue) {
  int element;
  if (queue->rear == (N-1))
    return;
  else {
    if (queue->front == -1) {
      queue->front = 0;
      element = input;
      queue->rear++;
      queue->q[queue->rear] = element;
    }
    else{
      element = input;
      queue->rear++;
      queue->q[queue->rear] = element;
    }
  }
}

void delete (Queue *queue) {
  if (queue->front == -1 || queue->front > queue->rear)
    return;
  else {
    queue->rear--;
  }
}


#if MODE == X86
void print_queue(int *queue) {
  int i;
  for(i = 1; i < N-1; i++)
    printf("%d =>", queue[i]);
  printf("%d", queue[i]);
}
#endif

#if MODE == RISCV

int main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  Queue solution[N][N];
  INF = 999;

  /* matrix initialization */

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        graph[i][j] = INF;

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;

  /* distance graph initialization */

  for (i = 1; i < N; i++) {
    graph[i][i + 1] = 1;
  }
  for (i = 1; i < N; i++) {
    via[i][i + 1] = i;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  /* solution initialization */
  
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {

      init_queue(&solution[i][j]);
      insert(i, &solution[i][j]);
    }
  }

  floyds(graph, N, via);

  /* find path for each node */
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      find_path(i, j, &solution[i][j], via);
    }
  }

  /* cost for each path */

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      solution[i][j].cost = graph[i][j];
    }
  }

}

#endif

#if MODE == X86
#if PRINT == FLOYD
/* check the floyd warshall algorithm on x86 */
void main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  INF = 999;

  /* matrix initialization */

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        graph[i][j] = INF;

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;

  /* distance graph initialization */

  for (i = 1; i < N; i++){
    graph[i][i+1] = 1;
  }

  printf("\n Matrix of input data:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

  floyds(graph, N);

  printf("\n distance graph:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

}


#elif PRINT == FIND_PATH
/* check the algorithm for kavin bacon problem on x86 */
void main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  INF = 999;

  /* matrix initialization */

  for (i = 1; i < N; i++) 
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        graph[i][j] = INF;

  for (i = 1; i < N; i++) 
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;

  /* distance graph initialization */

  for (i = 1; i < N; i++){
    graph[i][i+1] = 1;
  }
  for (i = 1; i < N; i++){
    via[i][i+1] = i;
  }
  graph[N-1][1] = 1;
  via[N-1][1] = N-1;

  floyds(graph, N, via);

  Queue solution[N][N];

  for (i = 1; i < N; i++){
    for (j = 1; j < N; j++){

      init_queue(&solution[i][j]);
      insert(i, &solution[i][j]);

    }
  }

  printf("\n distance graph:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

  printf("\n via path:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", via[i][j]);
    printf("\n");
  }

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
        find_path(i, j, &solution[i][j], via);
    }
  }

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      solution[i][j].cost = graph[i][j];
    }
  }

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      printf("solution[%d][%d]: ", i,j);
      print_queue(&solution[i][j]);
      printf("\n");

    }
    printf("\n");
  }
}
#endif
#endif