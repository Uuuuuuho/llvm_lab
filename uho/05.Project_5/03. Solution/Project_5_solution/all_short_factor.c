
#include "all_short.h"

//=============
//  COMMIT
//=============

void floyds(int p[][N], int n, int via[][N]) {
  int i, j, k;
#if UNROLL_FACTOR == 1  
  #if FACTOR == INNER_MOST
    int* t_1, *t_2, *t_3;
  #elif FACTOR == SECOND_LOOP
    int* t_1, *t_2, *t_3, *t_4;
  #endif
    for (k = 1; k < n; k++){
  #if FACTOR == SECOND_LOOP
      t_4 = &p[k];
  #endif
      for (i = 1; i < n; i++) {
  #if FACTOR == INNER_MOST
            t_1 = &p[i][k];
            t_2 = &p[i];
            t_3 = &via[i];
  #elif FACTOR == SECOND_LOOP
            t_1 = &p[i][k];
            t_2 = &p[i];
            t_3 = &via[i];
  #endif
        for (j = 1; j < n; j++) {
          if (i == j)
            *(t_2 + j) = INF;
          else {
  #if FACTOR == INNER_MOST
            if (*(t_2 + j) > *t_1 + p[k][j])
              *(t_3 + j) = k;

            *(t_2 + j) = min(*(t_2 + j), *t_1 + p[k][j]);
#elif FACTOR == SECOND_LOOP
            if (*(t_2 + j) > *t_1 + *(t_4 + j))
              *(t_3 + j) = k;

            *(t_2 + j) = min(*(t_2 + j), *t_1 + *(t_4 + j));
  #endif
        }
      }
    }
  }

#elif UNROLL_FACTOR == 2
  #if FACTOR == INNER_MOST
    int* t_1[2], *t_2[2], *t_3[2];
  #elif FACTOR == SECOND_LOOP
    int* t_1[2], *t_2[2], *t_3[2], *t_4;
  #endif

    for (k = 1; k < n; k++){
  #if FACTOR == SECOND_LOOP
      t_4 = &p[k];
  #endif
      for (i = 1; i < n; i += 2) {
  #if FACTOR == INNER_MOST
        t_1[0] = &p[i][k];
        t_1[1] = &p[i+1][k];
        t_2[0] = &p[i];
        t_2[1] = &p[i+1];
        t_3[0] = &via[i];
        t_3[1] = &via[i+1];
#elif FACTOR == SECOND_LOOP
        t_1[0] = &p[i][k];
        t_1[1] = &p[i+1][k];
        t_2[0] = &p[i];
        t_2[1] = &p[i+1];
        t_3[0] = &via[i];
        t_3[1] = &via[i+1];
  #endif
        for (j = 1; j < n; j += 2) {
#if FACTOR == INNER_MOST          
          if (i == j) {
            *(t_2[0] + j) = INF;
            *(t_2[1] + j+1) = INF;

            // for correctness
            if (*(t_2[0] + j + 1) > *t_1[0] + p[k][j + 1])
              *(t_3[0] + j + 1) = k;
            if (*(t_2[1] + j) > *t_1[1] + p[k][j])
              *(t_3[1] + j) = k;
            *(t_2[0] + j + 1) = min(*(t_2[0] + j + 1), *(t_2[0] + k) + p[k][j + 1]);
            *(t_2[1] + j) = min(*(t_2[1] + j), *(t_2[1] + k) + p[k][j]);
          }
          else {

            if (*(t_2[0] + j) > *t_1[0] + p[k][j])
              *(t_3[0] + j) = k;
            if (*(t_2[0] + j + 1) > *t_1[0] + p[k][j + 1])
              *(t_3[0] + j + 1) = k;
            if (*(t_2[1] + j) > *t_1[1] + p[k][j])
              *(t_3[1] + j) = k;
            if (*(t_2[1] + j + 1) > *t_1[1] + p[k][j + 1])
              *(t_3[1] + j + 1) = k;

            *(t_2[0] + j) = min(*(t_2[0] + j), *(t_2[0] + k) + p[k][j]);
            *(t_2[0] + j + 1) = min(*(t_2[0] + j + 1), *(t_2[0] + k) + p[k][j + 1]);
            *(t_2[1] + j) = min(*(t_2[1] + j), *(t_2[1] + k) + p[k][j]);
            *(t_2[1] + j + 1) =
                min(*(t_2[1] + j + 1), *(t_2[1] + k) + p[k][j + 1]);
          }
#elif FACTOR == SECOND_LOOP
          if (i == j) {
            *(t_2[0] + j) = INF;
            *(t_2[1] + j + 1) = INF;

            // for correctness
            if (*(t_2[0] + j + 1) > *t_1[0] + *(t_4 + j + 1))
              *(t_3[0] + j + 1) = k;
            if (*(t_2[1] + j) > *t_1[1] + *(t_4 + j))
              *(t_3[1] + j) = k;
            *(t_2[0] + j + 1) =
                min(*(t_2[0] + j + 1), *(t_2[0] + k) + *(t_4 + j + 1));
            *(t_2[1] + j) = min(*(t_2[1] + j), *(t_2[1] + k) + *(t_4 + j));
          } else {

            if (*(t_2[0] + j) > *t_1[0] + *(t_4 + j))
              *(t_3[0] + j) = k;
            if (*(t_2[0] + j + 1) > *t_1[0] + *(t_4 + j + 1))
              *(t_3[0] + j + 1) = k;
            if (*(t_2[1] + j) > *t_1[1] + *(t_4 + j))
              *(t_3[1] + j) = k;
            if (*(t_2[1] + j + 1) > *t_1[1] + *(t_4 + j + 1))
              *(t_3[1] + j + 1) = k;

            *(t_2[0] + j) = min(*(t_2[0] + j), *(t_2[0] + k) + *(t_4 + j));
            *(t_2[0] + j + 1) =
                min(*(t_2[0] + j + 1), *(t_2[0] + k) + *(t_4 + j + 1));
            *(t_2[1] + j) = min(*(t_2[1] + j), *(t_2[1] + k) + *(t_4 + j));
            *(t_2[1] + j + 1) =
                min(*(t_2[1] + j + 1), *(t_2[1] + k) + *(t_4 + j + 1));
          }
#endif          
        }
      }
    }
#elif UNROLL_FACTOR == 4
#if FACTOR == INNER_MOST
  int *t_1[4], *t_2[4], *t_3[4];
#elif FACTOR == SECOND_LOOP
  int *t_1[4], *t_2[4], *t_3[4], *t_4;
#endif
  for (k = 1; k < n; k++){
#if FACTOR == SECOND_LOOP
    t_4 = &p[k];
#endif
    for (i = 1; i < n; i += 4){
#if FACTOR == INNER_MOST
      t_1[0] = &p[i][k];
      t_1[1] = &p[i + 1][k];
      t_1[2] = &p[i + 2][k];
      t_1[3] = &p[i + 3][k];
      t_2[0] = &p[i];
      t_2[1] = &p[i + 1];
      t_2[2] = &p[i + 2];
      t_2[3] = &p[i + 3];
      t_3[0] = &via[i];
      t_3[1] = &via[i + 1];
      t_3[2] = &via[i + 2];
      t_3[3] = &via[i + 3];
#elif FACTOR == SECOND_LOOP
      t_1[0] = &p[i][k];
      t_1[1] = &p[i + 1][k];
      t_1[2] = &p[i + 2][k];
      t_1[3] = &p[i + 3][k];
      t_2[0] = &p[i];
      t_2[1] = &p[i + 1];
      t_2[2] = &p[i + 2];
      t_2[3] = &p[i + 3];
      t_3[0] = &via[i];
      t_3[1] = &via[i + 1];
      t_3[2] = &via[i + 2];
      t_3[3] = &via[i + 3];
#endif
      for (j = 1; j < n; j += 4) {
#if FACTOR == INNER_MOST        
        if (i == j) {

          *(t_2[0] + j) = INF;
          *(t_2[1] + j + 1) = INF;
          *(t_2[2] + j + 2) = INF;
          *(t_2[3] + j + 3) = INF;

          // for correctness
          if (*(t_2[0] + j + 1) > (*(t_1[0]) + p[k][j + 1]))
            *(t_3[0] + j + 1) = k;
          if (*(t_2[0] + j + 2) > (*(t_1[0]) + p[k][j + 2]))
            *(t_3[0] + j + 2) = k;
          if (*(t_2[0] + j + 3) > (*(t_1[0]) + p[k][j + 3]))
            *(t_3[0] + j + 3) = k;
          if (*(t_2[1] + j) > *(t_2[1] + k) + p[k][j])
            *(t_3[1] + j) = k;
          if (*(t_2[1] + j + 2) > *(t_2[1] + k) + p[k][j + 2])
            *(t_3[1] + j + 2) = k;
          if (*(t_2[1] + j + 3) > *(t_2[1] + k) + p[k][j + 3])
            *(t_3[1] + j + 3) = k;
          if (*(t_2[2] + j) > *(t_1[2]) + p[k][j])
            *(t_3[2] + j) = k;
          if (*(t_2[2] + j + 1) > *(t_1[2]) + p[k][j + 1])
            *(t_3[2] + j + 1) = k;
          if (*(t_2[2] + j + 3) > *(t_1[2]) + p[k][j + 3])
            *(t_3[2] + j + 3) = k;
          if (*(t_2[3] + j) > *(t_1[3]) + p[k][j])
            *(t_3[3] + j) = k;
          if (*(t_2[3] + j + 1) > *(t_1[3]) + p[k][j + 1])
            *(t_3[3] + j + 1) = k;
          if (*(t_2[3] + j + 2) > *(t_1[3]) + p[k][j + 2])
            *(t_3[3] + j + 2) = k;

          *(t_2[0] + j + 1) = min(*(t_2[0] + j + 1), *(t_1[0]) + p[k][j + 1]);
          *(t_2[0] + j + 2) = min(*(t_2[0] + j + 2), *(t_1[0]) + p[k][j + 2]);
          *(t_2[0] + j + 3) = min(*(t_2[0] + j + 3), *(t_1[0]) + p[k][j + 3]);
          *(t_2[1] + j) = min(*(t_2[1] + j), *(t_1[1]) + p[k][j]);
          *(t_2[1] + j + 2) = min(*(t_2[1] + j + 2), *(t_1[1]) + p[k][j + 2]);
          *(t_2[1] + j + 3) = min(*(t_2[1] + j + 3), *(t_1[1]) + p[k][j + 3]);
          *(t_2[2] + j) = min(*(t_2[2] + j), *(t_1[2]) + p[k][j]);
          *(t_2[2] + j + 1) = min(*(t_2[2] + j + 1), *(t_1[2]) + p[k][j + 1]);
          *(t_2[2] + j + 3) = min(*(t_2[2] + j + 3), *(t_1[2]) + p[k][j + 3]);
          *(t_2[3] + j) = min(*(t_2[3] + j), *(t_1[3]) + p[k][j]);
          *(t_2[3] + j + 1) = min(*(t_2[3] + j + 1), *(t_1[3]) + p[k][j + 1]);
          *(t_2[3] + j + 2) = min(*(t_2[3] + j + 2), *(t_1[3]) + p[k][j + 2]);

        } else {

          if (*(t_2[0] + j) > *(t_1[1]) + p[k][j])           *(t_3[0] + j) = k;
          if (*(t_2[0] + j + 1) > *(t_1[1]) + p[k][j + 1])   *(t_3[0] + j + 1) = k;
          if (*(t_2[0] + j + 2) > *(t_1[1]) + p[k][j + 2])   *(t_3[0] + j + 2) = k;
          if (*(t_2[0] + j + 3) > *(t_1[1]) + p[k][j + 3])   *(t_3[0] + j + 3) = k;
          if (*(t_2[1] + j) > *(t_1[1]) + p[k][j])             *(t_3[1] + j) = k;
          if (*(t_2[1] + j + 1) > *(t_1[1]) + p[k][j + 1])     *(t_3[1] + j + 1) = k;
          if (*(t_2[1] + j + 2) > *(t_1[1]) + p[k][j + 2])     *(t_3[1] + j + 2) = k;
          if (*(t_2[1] + j + 3) > *(t_1[1]) + p[k][j + 3])     *(t_3[1] + j + 3) = k;
          if (*(t_2[2] + j) > *(t_1[2]) + p[k][j])             *(t_3[2] + j) = k;
          if (*(t_2[2] + j + 1) > *(t_1[2]) + p[k][j + 1])     *(t_3[2] + j + 1) = k;
          if (*(t_2[2] + j + 2) > *(t_1[2]) + p[k][j + 2])     *(t_3[2] + j + 2) = k;
          if (*(t_2[2] + j + 3) > *(t_1[2]) + p[k][j + 3])     *(t_3[2] + j + 3) = k;
          if (*(t_2[3] + j) > *(t_1[3]) + p[k][j])             *(t_3[3] + j) = k;
          if (*(t_2[3] + j + 1) > *(t_1[3]) + p[k][j + 1])     *(t_3[3] + j + 1) = k;
          if (*(t_2[3] + j + 2) > *(t_1[3]) + p[k][j + 2])     *(t_3[3] + j + 2) = k;
          if (*(t_2[3] + j + 3) > *(t_1[3]) + p[k][j + 3])     *(t_3[3] + j + 3) = k;

          *(t_2[0] + j) = min(*(t_2[0] + j), *(t_1[1]) + p[k][j]);
          *(t_2[0] + j + 1) = min(*(t_2[0] + j + 1), *(t_1[1]) + p[k][j + 1]);
          *(t_2[0] + j + 2) = min(*(t_2[0] + j + 2), *(t_1[1]) + p[k][j + 2]);
          *(t_2[0] + j + 3) = min(*(t_2[0] + j + 3), *(t_1[1]) + p[k][j + 3]);
          *(t_2[1] + j) = min(*(t_2[1] + j), *(t_1[1]) + p[k][j]);
          *(t_2[1] + j + 1) = min(*(t_2[1] + j + 1), *(t_1[1]) + p[k][j + 1]);
          *(t_2[1] + j + 2) = min(*(t_2[1] + j + 2), *(t_1[1]) + p[k][j + 2]);
          *(t_2[1] + j + 3) = min(*(t_2[1] + j + 3), *(t_1[1]) + p[k][j + 3]);
          *(t_2[2] + j) = min(*(t_2[2] + j), *(t_1[2]) + p[k][j]);
          *(t_2[2] + j + 1) = min(*(t_2[2] = j + 1), *(t_1[2]) + p[k][j + 1]);
          *(t_2[2] + j + 2) = min(*(t_2[2] = j + 2), *(t_1[2]) + p[k][j + 2]);
          *(t_2[2] + j + 3) = min(*(t_2[2] = j + 3), *(t_1[2]) + p[k][j + 3]);
          *(t_2[3] + j) = min(*(t_2[3] + j), *(t_1[3]) + p[k][j]);
          *(t_2[3] = j + 1) = min(*(t_2[3] + j + 1), *(t_1[3]) + p[k][j + 1]);
          *(t_2[3] = j + 2) = min(*(t_2[3] + j + 2), *(t_1[3]) + p[k][j + 2]);
          *(t_2[3] = j + 3) = min(*(t_2[3] + j + 3), *(t_1[3]) + p[k][j + 3]);
        }
#elif FACTOR == SECOND_LOOP
        if (i == j) {

          *(t_2[0] + j) = INF;
          *(t_2[1] + j + 1) = INF;
          *(t_2[2] + j + 2) = INF;
          *(t_2[3] + j + 3) = INF;

          // for correctness

          if (*(t_2[0] + j + 1) > (*(t_1[0]) + *(t_4 + j + 1)))
            *(t_3[0] + j + 1) = k;
          if (*(t_2[0] + j + 2) > (*(t_1[0]) + *(t_4 + j + 2)))
            *(t_3[0] + j + 2) = k;
          if (*(t_2[0] + j + 3) > (*(t_1[0]) + *(t_4 + j + 3)))
            *(t_3[0] + j + 3) = k;
          if (*(t_2[1] + j) > *(t_2[1] + k) + *(t_4 + j))
            *(t_3[1] + j) = k;
          if (*(t_2[1] + j + 2) > *(t_2[1] + k) + *(t_4 + j + 2))
            *(t_3[1] + j + 2) = k;
          if (*(t_2[1] + j + 3) > *(t_2[1] + k) + *(t_4 + j + 3))
            *(t_3[1] + j + 3) = k;
          if (*(t_2[2] + j) > *(t_1[2]) + *(t_4 + j))
            *(t_3[2] + j) = k;
          if (*(t_2[2] + j + 1) > *(t_1[2]) + *(t_4 + j + 1))
            *(t_3[2] + j + 1) = k;
          if (*(t_2[2] + j + 3) > *(t_1[2]) + *(t_4 + j + 3))
            *(t_3[2] + j + 3) = k;
          if (*(t_2[3] + j) > *(t_1[3]) + *(t_4 + j))
            *(t_3[3] + j) = k;
          if (*(t_2[3] + j + 1) > *(t_1[3]) + *(t_4 + j + 1))
            *(t_3[3] + j + 1) = k;
          if (*(t_2[3] + j + 2) > *(t_1[3]) + *(t_4 + j + 2))
            *(t_3[3] + j + 2) = k;


          *(t_2[0] + j + 1) = min(*(t_2[0] + j + 1), *(t_1[0]) + *(t_4 + j + 1));
          *(t_2[0] + j + 2) = min(*(t_2[0] + j + 2), *(t_1[0]) + *(t_4 + j + 2));
          *(t_2[0] + j + 3) = min(*(t_2[0] + j + 3), *(t_1[0]) + *(t_4 + j + 3));
          *(t_2[1] + j) = min(*(t_2[1] + j), *(t_1[1]) + *(t_4 + j));
          *(t_2[1] + j + 2) = min(*(t_2[1] + j + 2), *(t_1[1]) + *(t_4 + j + 2));
          *(t_2[1] + j + 3) = min(*(t_2[1] + j + 3), *(t_1[1]) + *(t_4 + j + 3));
          *(t_2[2] + j) = min(*(t_2[2] + j), *(t_1[2]) + *(t_4 + j));
          *(t_2[2] + j + 1) = min(*(t_2[2] + j + 1), *(t_1[2]) + *(t_4 + j + 1));
          *(t_2[2] + j + 3) = min(*(t_2[2] + j + 3), *(t_1[2]) + *(t_4 + j + 3));
          *(t_2[3] + j) = min(*(t_2[3] + j), *(t_1[3]) + *(t_4 + j));
          *(t_2[3] + j + 1) = min(*(t_2[3] + j + 1), *(t_1[3]) + *(t_4 + j + 1));
          *(t_2[3] + j + 2) = min(*(t_2[3] + j + 2), *(t_1[3]) + *(t_4 + j + 2));

        } else {

          if (*(t_2[0] + j) > *(t_1[1]) + *(t_4 + j))           *(t_3[0] + j) = k;
          if (*(t_2[0] + j + 1) > *(t_1[1]) + *(t_4 + j + 1))   *(t_3[0] + j + 1) = k;
          if (*(t_2[0] + j + 2) > *(t_1[1]) + *(t_4 + j + 2))   *(t_3[0] + j + 2) = k;
          if (*(t_2[0] + j + 3) > *(t_1[1]) + *(t_4 + j + 3))   *(t_3[0] + j + 3) = k;
          if (*(t_2[1] + j) > *(t_1[1]) + *(t_4 + j))           *(t_3[1] + j) = k;
          if (*(t_2[1] + j + 1) > *(t_1[1]) + *(t_4 + j + 1))     *(t_3[1] + j + 1) = k;
          if (*(t_2[1] + j + 2) > *(t_1[1]) + *(t_4 + j + 2))     *(t_3[1] + j + 2) = k;
          if (*(t_2[1] + j + 3) > *(t_1[1]) + *(t_4 + j + 3))     *(t_3[1] + j + 3) = k;
          if (*(t_2[2] + j) > *(t_1[2]) + *(t_4 + j))             *(t_3[2] + j) = k;
          if (*(t_2[2] + j + 1) > *(t_1[2]) + *(t_4 + j + 1))     *(t_3[2] + j + 1) = k;
          if (*(t_2[2] + j + 2) > *(t_1[2]) + *(t_4 + j + 2))     *(t_3[2] + j + 2) = k;
          if (*(t_2[2] + j + 3) > *(t_1[2]) + *(t_4 + j + 3))     *(t_3[2] + j + 3) = k;
          if (*(t_2[3] + j) > *(t_1[3]) + *(t_4 + j))             *(t_3[3] + j) = k;
          if (*(t_2[3] + j + 1) > *(t_1[3]) + *(t_4 + j + 1))     *(t_3[3] + j + 1) = k;
          if (*(t_2[3] + j + 2) > *(t_1[3]) + *(t_4 + j + 2))     *(t_3[3] + j + 2) = k;
          if (*(t_2[3] + j + 3) > *(t_1[3]) + *(t_4 + j + 3))     *(t_3[3] + j + 3) = k;

          *(t_2[0] + j) = min(*(t_2[0] + j), *(t_1[1]) + *(t_4 + j));
          *(t_2[0] + j + 1) = min(*(t_2[0] + j + 1), *(t_1[1]) + *(t_4 + j + 1));
          *(t_2[0] + j + 2) = min(*(t_2[0] + j + 2), *(t_1[1]) + *(t_4 + j + 2));
          *(t_2[0] + j + 3) = min(*(t_2[0] + j + 3), *(t_1[1]) + *(t_4 + j + 3));
          *(t_2[1] + j) = min(*(t_2[1] + j), *(t_1[1]) + *(t_4 + j));
          *(t_2[1] + j + 1) = min(*(t_2[1] + j + 1), *(t_1[1]) + *(t_4 + j + 1));
          *(t_2[1] + j + 2) = min(*(t_2[1] + j + 2), *(t_1[1]) + *(t_4 + j + 2));
          *(t_2[1] + j + 3) = min(*(t_2[1] + j + 3), *(t_1[1]) + *(t_4 + j + 3));
          *(t_2[2] + j) = min(*(t_2[2] + j), *(t_1[2]) + *(t_4 + j));
          *(t_2[2] + j + 1) = min(*(t_2[2] + j + 1), *(t_1[2]) + *(t_4 + j + 1));
          *(t_2[2] + j + 2) = min(*(t_2[2] + j + 2), *(t_1[2]) + *(t_4 + j + 2));
          *(t_2[2] + j + 3) = min(*(t_2[2] + j + 3), *(t_1[2]) + *(t_4 + j + 3));
          *(t_2[3] + j) = min(*(t_2[3] + j), *(t_1[3]) + *(t_4 + j));
          *(t_2[3] + j + 1) = min(*(t_2[3] + j + 1), *(t_1[3]) + *(t_4 + j + 1));
          *(t_2[3] + j + 2) = min(*(t_2[3] + j + 2), *(t_1[3]) + *(t_4 + j + 2));
          *(t_2[3] + j + 3) = min(*(t_2[3] + j + 3), *(t_1[3]) + *(t_4 + j + 3));
        }
#endif
      }
    }
  }
#endif
}

int min(int a, int b) {
    if (a < b)
      return (a);
    else
      return (b);
}

void find_path(int i, int j, Queue *sol, int via[][N]) {
  if ((via[i][j]) && (i != via[i][j])) {
    find_path(i, via[i][j], sol, via);
    delete (sol);
    find_path(via[i][j], j, sol, via);
  }

  else {
    insert(i, sol);
    insert(j, sol);
    return;
  }
}

void init_queue(Queue *queue) {
  int i, j, k;
  for (i = 1; i < N; i++) {
    queue->q[i] = -1;
    queue->front = -1;
    queue->rear = -1;
    queue->cost = INF;
  }
}

void insert(int input, Queue *queue) {
  int element;
  if (queue->rear == (N - 1))
    return;
  else {
    if (queue->front == -1) {
      queue->front = 0;
      element = input;
      queue->rear++;
      queue->q[queue->rear] = element;
    } else {
      element = input;
      queue->rear++;
      queue->q[queue->rear] = element;
    }
  }
}

void delete (Queue *queue) {
  if (queue->front == -1 || queue->front > queue->rear)
    return;
  else {
    queue->rear--;
  }
}

#if MODE == X86
void print_queue(int *queue) {
  int i;
  for (i = 1; i < N - 1; i++)
    printf("%d =>", queue[i]);
  printf("%d", queue[i]);
}
#endif

#if MODE == RISCV
int main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  Queue solution[N][N];
  INF = 999;

  /* matrix initialization */

#if UNROLL_FACTOR == 1
  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        graph[i][j] = INF;

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;

  /* distance graph initialization */

  for (i = 1; i < N; i++) {
    graph[i][i + 1] = 1;
  }
  for (i = 1; i < N; i++) {
    via[i][i + 1] = i;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  /* solution initialization */

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {

      init_queue(&solution[i][j]);
      insert(i, &solution[i][j]);
    }
  }

  floyds(graph, N, via);

  /* find path for each node */
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      find_path(i, j, &solution[i][j], via);
    }
  }

  /* cost for each path */

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      solution[i][j].cost = graph[i][j];
    }
  }

#elif UNROLL_FACTOR == 2
  for (i = 1; i < N; i += 2)
    for (j = 1; j < N; j += 2)
      for (k = 1; k < N; k += 2) {
        graph[i][j] = INF;
        graph[i][j + 1] = INF;
        graph[i + 1][j] = INF;
        graph[i + 1][j + 1] = INF;

      } // useless loop

  for (i = 1; i < N; i += 2)
    for (j = 1; j < N; j += 2)
      for (k = 1; k < N; k += 2) {
        via[i][j] = FALSE;
        via[i][j + 1] = FALSE;
        via[i + 1][j] = FALSE;
        via[i + 1][j + 1] = FALSE;

      } // useless loop

  /* distance graph initialization */

  for (i = 1; i < N; i += 2) {
    graph[i][i + 1] = 1;
    graph[i + 1][i + 2] = 1;
  }
  for (i = 1; i < N; i += 2) {
    via[i][i + 1] = i;
    via[i + 1][i + 2] = i + 1;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  /* solution initialization */

  for (i = 1; i < N; i += 2) {
    for (j = 1; j < N; j += 2) {

      init_queue(&solution[i][j]);
      init_queue(&solution[i][j + 1]);
      init_queue(&solution[i + 1][j]);
      init_queue(&solution[i][j + 1]);
      insert(i, &solution[i][j]);
      insert(i, &solution[i][j + 1]);
      insert(i + 1, &solution[i + 1][j]);
      insert(i + 1, &solution[i][j + 1]);
    }
  }

  floyds(graph, N, via);

  /* find path for each node */
  for (i = 1; i < N; i += 2) {
    for (j = 1; j < N; j += 2) {
      find_path(i, j, &solution[i][j], via);
      find_path(i, j + 1, &solution[i][j + 1], via);
      find_path(i + 1, j, &solution[i + 1][j], via);
      find_path(i + 1, j + 1, &solution[i + 1][j + 1], via);
    }
  }

  /* cost for each path */

  for (i = 1; i < N; i += 2) {
    for (j = 1; j < N; j += 2) {
      solution[i][j].cost = graph[i][j];
      solution[i][j + 1].cost = graph[i][j + 1];
      solution[i + 1][j].cost = graph[i + 1][j];
      solution[i + 1][j + 1].cost = graph[i + 1][j + 1];
    }
  }

#elif UNROLL_FACTOR == 4

  for (i = 1; i < N; i += 4)
    for (j = 1; j < N; j += 4)
      for (k = 1; k < N; k += 4) {
        graph[i][j] = INF;
        graph[i][j + 1] = INF;
        graph[i][j + 2] = INF;
        graph[i][j + 3] = INF;
        graph[i + 1][j] = INF;
        graph[i + 1][j + 1] = INF;
        graph[i + 1][j + 2] = INF;
        graph[i + 1][j + 3] = INF;
        graph[i + 2][j] = INF;
        graph[i + 2][j + 1] = INF;
        graph[i + 2][j + 2] = INF;
        graph[i + 2][j + 3] = INF;
        graph[i + 3][j] = INF;
        graph[i + 3][j + 1] = INF;
        graph[i + 3][j + 2] = INF;
        graph[i + 3][j + 3] = INF;

      } // useless loop

  for (i = 1; i < N; i += 4)
    for (j = 1; j < N; j += 4)
      for (k = 1; k < N; k += 4) {
        via[i][j] = FALSE;
        via[i][j + 1] = FALSE;
        via[i][j + 2] = FALSE;
        via[i][j + 3] = FALSE;
        via[i + 1][j] = FALSE;
        via[i + 1][j + 1] = FALSE;
        via[i + 1][j + 2] = FALSE;
        via[i + 1][j + 3] = FALSE;
        via[i + 2][j] = FALSE;
        via[i + 2][j + 1] = FALSE;
        via[i + 2][j + 2] = FALSE;
        via[i + 2][j + 3] = FALSE;
        via[i + 3][j] = FALSE;
        via[i + 3][j + 1] = FALSE;
        via[i + 3][j + 2] = FALSE;
        via[i + 3][j + 3] = FALSE;

      } // useless loop

  /* distance graph initialization */

  for (i = 1; i < N; i += 4) {
    graph[i][i + 1] = 1;
    graph[i + 1][i + 2] = 1;
    graph[i + 2][i + 3] = 1;
    graph[i + 3][i + 4] = 1;
  }
  for (i = 1; i < N; i += 4) {
    via[i][i + 1] = i;
    via[i + 1][i + 2] = i + 1;
    via[i + 2][i + 3] = i + 2;
    via[i + 3][i + 4] = i + 3;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  /* solution initialization */

  for (i = 1; i < N; i += 4) {
    for (j = 1; j < N; j += 4) {

      init_queue(&solution[i][j]);
      init_queue(&solution[i][j + 1]);
      init_queue(&solution[i][j + 2]);
      init_queue(&solution[i][j + 3]);
      init_queue(&solution[i + 1][j]);
      init_queue(&solution[i + 1][j + 1]);
      init_queue(&solution[i + 1][j + 2]);
      init_queue(&solution[i + 1][j + 3]);
      init_queue(&solution[i + 2][j]);
      init_queue(&solution[i + 2][j + 1]);
      init_queue(&solution[i + 2][j + 2]);
      init_queue(&solution[i + 2][j + 3]);
      init_queue(&solution[i + 3][j]);
      init_queue(&solution[i + 3][j + 1]);
      init_queue(&solution[i + 3][j + 2]);
      init_queue(&solution[i + 3][j + 3]);
    }
  }
  for (i = 1; i < N; i += 4) {
    for (j = 1; j < N; j += 4) {
      insert(i, &solution[i][j]);
      insert(i, &solution[i][j + 1]);
      insert(i, &solution[i][j + 2]);
      insert(i, &solution[i][j + 3]);
      insert(i, &solution[i + 1][j]);
      insert(i, &solution[i + 1][j + 1]);
      insert(i, &solution[i + 1][j + 2]);
      insert(i, &solution[i + 1][j + 3]);
      insert(i, &solution[i + 2][j]);
      insert(i, &solution[i + 2][j + 1]);
      insert(i, &solution[i + 2][j + 2]);
      insert(i, &solution[i + 2][j + 3]);
      insert(i, &solution[i + 3][j]);
      insert(i, &solution[i + 3][j + 1]);
      insert(i, &solution[i + 3][j + 2]);
      insert(i, &solution[i + 3][j + 3]);
    }
  }

  floyds(graph, N, via);

  /* find path for each node */
  for (i = 1; i < N; i += 4) {
    for (j = 1; j < N; j += 4) {
      find_path(i, j, &solution[i][j], via);
      find_path(i, j + 1, &solution[i][j + 1], via);
      find_path(i, j + 2, &solution[i][j + 2], via);
      find_path(i, j + 3, &solution[i][j + 3], via);
      find_path(i + 1, j, &solution[i][j], via);
      find_path(i + 1, j + 1, &solution[i][j + 1], via);
      find_path(i + 1, j + 2, &solution[i][j + 2], via);
      find_path(i + 1, j + 3, &solution[i][j + 3], via);
      find_path(i + 2, j, &solution[i][j], via);
      find_path(i + 2, j + 1, &solution[i][j + 1], via);
      find_path(i + 2, j + 2, &solution[i][j + 2], via);
      find_path(i + 2, j + 3, &solution[i][j + 3], via);
      find_path(i + 3, j, &solution[i][j], via);
      find_path(i + 3, j + 1, &solution[i][j + 1], via);
      find_path(i + 3, j + 2, &solution[i][j + 2], via);
      find_path(i + 3, j + 3, &solution[i][j + 3], via);
    }
  }

  /* cost for each path */

  for (i = 1; i < N; i += 4) {
    for (j = 1; j < N; j += 4) {
      solution[i][j].cost = graph[i][j];
      solution[i][j + 1].cost = graph[i][j + 1];
      solution[i][j + 2].cost = graph[i][j + 2];
      solution[i][j + 3].cost = graph[i][j + 3];
      solution[i + 1][j].cost = graph[i + 1][j];
      solution[i + 1][j + 1].cost = graph[i + 1][j + 1];
      solution[i + 1][j + 2].cost = graph[i + 1][j + 2];
      solution[i + 1][j + 3].cost = graph[i + 1][j + 3];
      solution[i + 2][j].cost = graph[i + 2][j];
      solution[i + 2][j + 1].cost = graph[i + 2][j + 1];
      solution[i + 2][j + 2].cost = graph[i + 2][j + 2];
      solution[i + 2][j + 3].cost = graph[i + 2][j + 3];
      solution[i + 3][j].cost = graph[i + 3][j];
      solution[i + 3][j + 1].cost = graph[i + 3][j + 1];
      solution[i + 3][j + 2].cost = graph[i + 3][j + 2];
      solution[i + 3][j + 3].cost = graph[i + 3][j + 3];
    }
  }
#endif
}
#endif


#if MODE == X86
#if PRINT == FLOYD
/* check the floyd warshall algorithm on x86 */
void main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  INF = 999;
  /* matrix initialization */

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        graph[i][j] = INF;

  for (i = 1; i < N; i++)
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        via[i][j] = FALSE;

  /* distance graph initialization */

  for (i = 1; i < N; i++) {
    graph[i][i + 1] = 1;
  }

  printf("\n Matrix of input data:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

  floyds(graph, N);

  printf("\n distance graph:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }
}

#elif PRINT == FIND_PATH
/* check the algorithm for kavin bacon problem on x86 */
void main() {
  int graph[N][N], i, j, k;
  int via[N][N];
  INF = 999;

  /* matrix initialization */


#if FACTOR == INNER_MOST
  int *t_1, *t_2;
#elif FACTOR == SECOND_LOOP
  int *t_1, *t_2;
#endif

  for (i = 1; i < N; i++){
    t_1 = &graph[i];
    for (j = 1; j < N; j++){
      t_2 = (t_1 + j);
      for (k = 1; k < N; k++) // useless loop
        *(t_2) = INF;
        // graph[i][j] = INF;
    }
  }

  for (i = 1; i < N; i++){
    t_1 = &via[i];
    for (j = 1; j < N; j++)
      for (k = 1; k < N; k++) // useless loop
        *(t_1 + j) = FALSE;
        // via[i][j] = FALSE;
  }

  /* distance graph initialization */

  for (i = 1; i < N; i++) {
    graph[i][i + 1] = 1;
  }
  for (i = 1; i < N; i++) {
    via[i][i + 1] = i;
  }
  graph[N - 1][1] = 1;
  via[N - 1][1] = N - 1;

  floyds(graph, N, via);

  Queue solution[N][N];

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      init_queue(&solution[i][j]);
      insert(i, &solution[i][j]);
    }
  }

  printf("\n distance graph:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", graph[i][j]);
    printf("\n");
  }

  printf("\n via path:\n");
  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++)
      printf("%d \t", via[i][j]);
    printf("\n");
  }

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      find_path(i, j, &solution[i][j], via);
    }
  }

  for (i = 1; i < N; i++) {
    t_1 = &graph[i];
    for (j = 1; j < N; j++) {
      solution[i][j].cost = *(t_1 + j);
    }
  }

  for (i = 1; i < N; i++) {
    for (j = 1; j < N; j++) {
      printf("solution[%d][%d]: ", i, j);
      print_queue(&solution[i][j]);
      printf("\n");
    }
    printf("\n");
  }
}

#endif
#endif