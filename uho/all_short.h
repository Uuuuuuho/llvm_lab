//=============
//  COMMIT
//=============
#define X86 1
#define RISCV 2
#define MODE X86

#define FIND_PATH 0
#define FLOYD 1
#define NONE 2
#define PRINT FIND_PATH

#if MODE == X86
#include <stdio.h>
#include <stdlib.h>
#endif

#define N 5

#define TRUE 1
#define FALSE 0

typedef struct{
 int q[N];
 int front;
 int rear;
 int cost;
} Queue;

void init_queue(Queue *queue);
void insert(int input, Queue *queue);
void delete (Queue *queue);
void print_queue(int *queue);

int min(int, int);
void floyds(int p[][N], int n, int via[][N]);
void find_path(int i, int j, Queue* sol, int via[][N]);
int min(int a, int b);

/* PLEASE use these preprocess definition below... */

// default unroll factor: 1
#define UNROLL_FACTOR 4

#define INNER_MOST 1
#define SECOND_LOOP 2
#define FACTOR SECOND_LOOP

#define FUSION FALSE
#define INLINE FALSE

#define DEAD_CODE FALSE
#define CONST_PROP FALSE

#if CONST_PROP == FALSE
int INF;
#endif

#if CONST_PROP == TRUE
#define INF 999
#endif