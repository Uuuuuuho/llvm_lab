#include <stdio.h>

#define LOOP 64
#define IN_SIZE 100
#define WGT_SIZE 10
#define OUT_SIZE 100
#define STRIDE 1
#define C_END 10

int in[IN_SIZE] = {3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                   3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                   3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                   3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                   3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3};

int wgt[WGT_SIZE] = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
int out[OUT_SIZE] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void conv(int bias);

int main() {
  int bias = 2;
  conv(bias);  
  // return res;
  return 0;
}

void conv(int bias){
  int result = 0;
  int i = 0;
  int j = 0;
  int input = 0;
  int weight = 0;
  // int in[IN_SIZE] = { };

  // int wgt[WGT_SIZE] = { };
  // // int wgt[WGT_SIZE] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
  // int out[OUT_SIZE] = {};
                       
  for(i = 0; i < C_END; i+=STRIDE){
    for(j = 0; j < WGT_SIZE; j++){
      out[i] = in[i + j] * wgt[j] + out[i];
      // out[i] = in[i + j] / wgt[j] + out[i];
    }
    out[i] += bias;
  }

  for(i = 0; i < C_END; i+=STRIDE)
      printf("out[%d]: %d\n", i, out[i]);
}


