#define LOOP 64

int loop_test() {
  int result = 0;
  int a[LOOP];

  for (int i = 0; i < LOOP; i += 8)
    a[i] = i;

  for (int i = 0; i < LOOP; i += 8)
    result = result + a[i];
  
  return result;
}

int redundant(int red_1) {
  int red_2, red_3;
  red_2 = 2;
  red_3 = red_1 + red_2;
  return red_3;
}

// int main() {
__attribute__((optnone)) int main() {
  int ret = loop_test();  
  int res = redundant(ret);
  return res;
}
