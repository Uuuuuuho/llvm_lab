/* 
    DFS HEADER FILE
 */
#define START 0
#define NUM_VERTEX 10
#define LIMIT 12
//#define DEBUG 
int color [NUM_VERTEX];
/* for data size checking */
// int adj_graph[NUM_VERTEX][NUM_VERTEX];
int adj_graph[NUM_VERTEX][NUM_VERTEX] = {
   11,11,11,11,11,11,11,11,11,11,
   11,11,11,11,11,11,11,11,11,11,
   11,11,11,11,11,11,11,11,11,11,
   11,11,11,11,11,11,11,11,11,11,
   11,11,11,11,11,11,11,11,11,11,
   11,11,11,11,11,11,11,11,11,11,
   11,11,11,11,11,11,11,11,11,11,
   11,11,11,11,11,11,11,11,11,11,
   11,11,11,11,11,11,11,11,11,11,
   11,11,11,11,11,11,11,11,11,11
};
int queue[LIMIT]; // Array implementation of queue
int front, rear; // To insert and delete the data elements in the queue respectively
int i, j; // To traverse the loop to while displaying the stack


/* 
   pre-declaration	
*/

char vertex_name(int offset);
void dfs();
void dfs_search(int start);
void insert(int input);
void delete (int input);