
#include "DFS.h"


char vertex_name(int offset) {
  const int base = 65;
  return (base + offset);
}

void dfs(){
	int i;
	for(i = 0; i < NUM_VERTEX; i++)
		color[i] = 0;//White
	dfs_search(START);
	  for(i = 0; i < NUM_VERTEX; i++){
		if(color[i] == 0){//white
			dfs_search(i);
		}
	}
}

void dfs_search(int start){
	color[start] = 1;//gray
	int i;
	for(i = 0; i < NUM_VERTEX; i++){
		if(adj_graph[start][i] && color[i] == 0){
			dfs_search(i);
		}
	}
	color[start] = 2;
}


void insert(int input)
{
	int element;
	if (rear == LIMIT - 1) return;
	else {
	  if (front == - 1)
	  {
		  front = 0;
	      element = input;
	      rear++;
	      queue[rear] = element;
	  }
    }
}
void delete(int input)
{
  if (front == - 1 || front > rear) return;
  else{
   front++;
  }
}

int main()
{
  front = -1;
  rear = -1;

  for(i = 0; i < NUM_VERTEX; i++){
		for(j = 0; j < NUM_VERTEX; j++){
			adj_graph[i][j] = 0;
		}
  }

  int div_val, q_val;
  div_val= 3;
  q_val= 3;
  
  	// adj_graph[0][1] = div_val/q_val;
    adj_graph[0][2] = 1;
    adj_graph[0][8] = 1;
    adj_graph[0][9] = div_val/q_val;
    
    adj_graph[1][2] = div_val/q_val;
    adj_graph[1][3] = div_val/q_val;
    
    adj_graph[2][3] = div_val/q_val;
    adj_graph[2][4] = div_val/q_val;    
    
    adj_graph[3][5] = 1;
    adj_graph[3][4] = 1;    

    adj_graph[4][6] = 1;
    adj_graph[4][5] = 1;

    adj_graph[5][6] = 1;
    adj_graph[5][7] = 1;

    adj_graph[6][2] = 1;
    adj_graph[6][3] = 1;

    adj_graph[7][5] = 1;
    adj_graph[7][9] = 1;

    adj_graph[8][7] = 1;
    adj_graph[8][1] = 1;

    adj_graph[9][4] = 1;
    adj_graph[9][3] = 1;
  // for(i = 0; i < 3; i++){
	// 	for(j = 0; j < 4; j++){
	// 		adj_graph[i][j] = 0;
	// 	}
  // }

  dfs();
  // printf("BSF\n");
  // bfs();
}