#define N 128

void matmul (int mat1[][N], int mat2[][N], int res[][N]);


__attribute__((optnone)) int main(){
    int mat_a[N], mat_b[N], mat_res[N];
    int c;

    // c = a + b;

    matmul(mat_a, mat_b, mat_res);
    c = mat_res[0];
}

void matmul (int mat1[][N], int mat2[][N], int res[][N]){
    int i, j, k;
    // int weight = 4;

    for (i = 0; i < N; i++){
        for (j = 0; j < N; j++){
            res[i][j] = 0;
            for (k = 0; k < N; k++){
                if(mat1[i][k] == 0 || mat2[k][j] == 0)
                    continue; // skip zero elements
                /* computation */
                // res[i][j] += (mat1[i][k] * weight);
                res[i][j] += (mat1[i][k] * mat2[k][j]);
            }   
        }   
    }   

}