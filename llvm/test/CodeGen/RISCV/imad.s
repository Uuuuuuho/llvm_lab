	.text
	.attribute	4, 16
	.attribute	5, "rv32i2p0_m2p0"
	.file	"imad.ll"
	.globl	mul                             # -- Begin function mul
	.p2align	2
	.type	mul,@function
mul:                                    # @mul
# %bb.0:
	imad	a0, a0, a1, a1
	ret
.Lfunc_end0:
	.size	mul, .Lfunc_end0-mul
                                        # -- End function
	.globl	addi                            # -- Begin function addi
	.p2align	2
	.type	addi,@function
addi:                                   # @addi
# %bb.0:
	addi	a0, a0, 1
	ret
.Lfunc_end1:
	.size	addi, .Lfunc_end1-addi
                                        # -- End function
	.globl	slti                            # -- Begin function slti
	.p2align	2
	.type	slti,@function
slti:                                   # @slti
# %bb.0:
	slti	a0, a0, 2
	ret
.Lfunc_end2:
	.size	slti, .Lfunc_end2-slti
                                        # -- End function
	.globl	sltiu                           # -- Begin function sltiu
	.p2align	2
	.type	sltiu,@function
sltiu:                                  # @sltiu
# %bb.0:
	sltiu	a0, a0, 3
	ret
.Lfunc_end3:
	.size	sltiu, .Lfunc_end3-sltiu
                                        # -- End function
	.globl	xori                            # -- Begin function xori
	.p2align	2
	.type	xori,@function
xori:                                   # @xori
# %bb.0:
	xori	a0, a0, 4
	ret
.Lfunc_end4:
	.size	xori, .Lfunc_end4-xori
                                        # -- End function
	.globl	ori                             # -- Begin function ori
	.p2align	2
	.type	ori,@function
ori:                                    # @ori
# %bb.0:
	ori	a0, a0, 5
	ret
.Lfunc_end5:
	.size	ori, .Lfunc_end5-ori
                                        # -- End function
	.globl	andi                            # -- Begin function andi
	.p2align	2
	.type	andi,@function
andi:                                   # @andi
# %bb.0:
	andi	a0, a0, 6
	ret
.Lfunc_end6:
	.size	andi, .Lfunc_end6-andi
                                        # -- End function
	.globl	slli                            # -- Begin function slli
	.p2align	2
	.type	slli,@function
slli:                                   # @slli
# %bb.0:
	slli	a0, a0, 7
	ret
.Lfunc_end7:
	.size	slli, .Lfunc_end7-slli
                                        # -- End function
	.globl	srli                            # -- Begin function srli
	.p2align	2
	.type	srli,@function
srli:                                   # @srli
# %bb.0:
	srli	a0, a0, 8
	ret
.Lfunc_end8:
	.size	srli, .Lfunc_end8-srli
                                        # -- End function
	.globl	srli_demandedbits               # -- Begin function srli_demandedbits
	.p2align	2
	.type	srli_demandedbits,@function
srli_demandedbits:                      # @srli_demandedbits
	.cfi_startproc
# %bb.0:
	srli	a0, a0, 3
	ori	a0, a0, 1
	ret
.Lfunc_end9:
	.size	srli_demandedbits, .Lfunc_end9-srli_demandedbits
	.cfi_endproc
                                        # -- End function
	.globl	srai                            # -- Begin function srai
	.p2align	2
	.type	srai,@function
srai:                                   # @srai
# %bb.0:
	srai	a0, a0, 9
	ret
.Lfunc_end10:
	.size	srai, .Lfunc_end10-srai
                                        # -- End function
	.globl	add                             # -- Begin function add
	.p2align	2
	.type	add,@function
add:                                    # @add
# %bb.0:
	add	a0, a0, a1
	ret
.Lfunc_end11:
	.size	add, .Lfunc_end11-add
                                        # -- End function
	.globl	sub                             # -- Begin function sub
	.p2align	2
	.type	sub,@function
sub:                                    # @sub
# %bb.0:
	sub	a0, a0, a1
	ret
.Lfunc_end12:
	.size	sub, .Lfunc_end12-sub
                                        # -- End function
	.globl	sub_negative_constant_lhs       # -- Begin function sub_negative_constant_lhs
	.p2align	2
	.type	sub_negative_constant_lhs,@function
sub_negative_constant_lhs:              # @sub_negative_constant_lhs
# %bb.0:
	addi	a1, zero, -2
	sub	a0, a1, a0
	ret
.Lfunc_end13:
	.size	sub_negative_constant_lhs, .Lfunc_end13-sub_negative_constant_lhs
                                        # -- End function
	.globl	sll                             # -- Begin function sll
	.p2align	2
	.type	sll,@function
sll:                                    # @sll
# %bb.0:
	sll	a0, a0, a1
	ret
.Lfunc_end14:
	.size	sll, .Lfunc_end14-sll
                                        # -- End function
	.globl	sll_negative_constant_lhs       # -- Begin function sll_negative_constant_lhs
	.p2align	2
	.type	sll_negative_constant_lhs,@function
sll_negative_constant_lhs:              # @sll_negative_constant_lhs
# %bb.0:
	addi	a1, zero, -1
	sll	a0, a1, a0
	ret
.Lfunc_end15:
	.size	sll_negative_constant_lhs, .Lfunc_end15-sll_negative_constant_lhs
                                        # -- End function
	.globl	slt                             # -- Begin function slt
	.p2align	2
	.type	slt,@function
slt:                                    # @slt
# %bb.0:
	slt	a0, a0, a1
	ret
.Lfunc_end16:
	.size	slt, .Lfunc_end16-slt
                                        # -- End function
	.globl	sltu                            # -- Begin function sltu
	.p2align	2
	.type	sltu,@function
sltu:                                   # @sltu
# %bb.0:
	sltu	a0, a0, a1
	ret
.Lfunc_end17:
	.size	sltu, .Lfunc_end17-sltu
                                        # -- End function
	.globl	xor                             # -- Begin function xor
	.p2align	2
	.type	xor,@function
xor:                                    # @xor
# %bb.0:
	xor	a0, a0, a1
	ret
.Lfunc_end18:
	.size	xor, .Lfunc_end18-xor
                                        # -- End function
	.globl	srl                             # -- Begin function srl
	.p2align	2
	.type	srl,@function
srl:                                    # @srl
# %bb.0:
	srl	a0, a0, a1
	ret
.Lfunc_end19:
	.size	srl, .Lfunc_end19-srl
                                        # -- End function
	.globl	srl_negative_constant_lhs       # -- Begin function srl_negative_constant_lhs
	.p2align	2
	.type	srl_negative_constant_lhs,@function
srl_negative_constant_lhs:              # @srl_negative_constant_lhs
# %bb.0:
	addi	a1, zero, -1
	srl	a0, a1, a0
	ret
.Lfunc_end20:
	.size	srl_negative_constant_lhs, .Lfunc_end20-srl_negative_constant_lhs
                                        # -- End function
	.globl	sra                             # -- Begin function sra
	.p2align	2
	.type	sra,@function
sra:                                    # @sra
# %bb.0:
	sra	a0, a0, a1
	ret
.Lfunc_end21:
	.size	sra, .Lfunc_end21-sra
                                        # -- End function
	.globl	sra_negative_constant_lhs       # -- Begin function sra_negative_constant_lhs
	.p2align	2
	.type	sra_negative_constant_lhs,@function
sra_negative_constant_lhs:              # @sra_negative_constant_lhs
# %bb.0:
	lui	a1, 524288
	sra	a0, a1, a0
	ret
.Lfunc_end22:
	.size	sra_negative_constant_lhs, .Lfunc_end22-sra_negative_constant_lhs
                                        # -- End function
	.globl	or                              # -- Begin function or
	.p2align	2
	.type	or,@function
or:                                     # @or
# %bb.0:
	or	a0, a0, a1
	ret
.Lfunc_end23:
	.size	or, .Lfunc_end23-or
                                        # -- End function
	.globl	and                             # -- Begin function and
	.p2align	2
	.type	and,@function
and:                                    # @and
# %bb.0:
	and	a0, a0, a1
	ret
.Lfunc_end24:
	.size	and, .Lfunc_end24-and
                                        # -- End function
	.section	".note.GNU-stack","",@progbits
